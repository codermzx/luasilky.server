function 取帮派数量()
	local 帮派数量 = 0

	for n, v in pairs(帮派数据) do
		if 帮派数据[n] ~= nil then
			帮派数量 = 帮派数量 + 1
		end
	end

	return 帮派数量
end



-- function __S服务:启动成功()
-- 	return 0
-- end

function 取管理ip(ip)
	for n = 1, #服务端参数.管理ip do
		if 服务端参数.管理ip[n] == ip then
			return true
		end
	end

	return false
end




function 账号取id(账号)
	for n, v in pairs(玩家数据) do
		if 玩家数据[n].账号 == 账号 then
			return n
		end
	end

	return 0
end

function 取队伍飞升状态(id)
	for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
		if 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].角色.数据.飞升 ~= true then
			return false
		end
	end

	return true
end

function 取队伍符合等级(id, 等级)
	local 符合等级 = {}

	if 玩家数据[id].队伍 == 0 then
		符合等级[#符合等级 + 1] = 玩家数据[id].角色.数据.等级
	else
		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			符合等级[#符合等级 + 1] = 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].角色.数据.等级
		end
	end

	for n = 1, #符合等级 do
		if 符合等级[n] < 等级 then
			return false
		end
	end

	return true
end

function 取队伍高于等级(id, 等级)
	local 符合等级 = {}

	if 玩家数据[id].队伍 == 0 then
		符合等级[#符合等级 + 1] = 玩家数据[id].角色.数据.等级
	else
		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			符合等级[#符合等级 + 1] = 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].角色.数据.等级
		end
	end

	for n = 1, #符合等级 do
		if 等级 < 符合等级[n] then
			return false
		end
	end

	return true
end

function 取队伍人数(id)
	if 玩家数据[id].队伍 == 0 then
		return 1
	else
		return #队伍数据[玩家数据[id].队伍].队员数据
	end
end



function 取帮派加人权限(id, 编号)
	if 编号 == nil or 帮派数据[编号] == nil then
		return false
	else
		local 临时职务 = 帮派数据[编号].成员名单[id].职务

		if 临时职务 == "帮主" or 临时职务 == "副帮主" or 临时职务 == "长老" then
			return true
		else
			return false
		end
	end
end

function 取帮派踢人权限(id, 编号)
	if 编号 == nil or 帮派数据[编号] == nil then
		return false
	else
		local 临时职务 = 帮派数据[编号].成员名单[id].职务

		if 临时职务 == "帮主" or 临时职务 == "副帮主" then
			return true
		else
			return false
		end
	end
end



function 取随机书铁()
	if 取随机数() <= 40 then
		return "制造指南书"
	else
		return "百炼精铁"
	end
end

function 取随机制造()
	if 取随机数() <= 50 then
		return 制造装备.武器.类型[取随机数(1, 12)]
	else
		return 制造装备.防具[取随机数(1, 7)]
	end
end

function 取角色等级(id)
	return 玩家数据[id].角色.数据.等级
end

function 取角色银子(id)
	return 玩家数据[id].角色.数据.道具.货币.银子
end

function 取角色经验(id)
	return 玩家数据[id].角色.数据.当前经验
end


function 银子检查(id, 数额, 储备)
	if 储备 == nil then
		if 玩家数据[id].角色.数据.道具.货币.银子 < 数额 then
			return false
		else
			return true
		end
	elseif 数额 > 玩家数据[id].角色.数据.道具.货币.银子 + 玩家数据[id].角色.数据.道具.货币.储备 then
		return false
	else
		return true
	end
end




function 更新存档信息()
	local 存档玩家组1 = 读入文件("/存档名称.txt")
	存档玩家组 = 分割文本(存档玩家组1, "*-*")
	玩家数据记录 = ""
	帮派更换数量 = 0

	for n = 1, #存档玩家组 do
		__S服务:输出("进度:" .. n .. "/" .. #存档玩家组 .. "/" .. 存档玩家组[n])

		if f函数.文件是否存在(程序目录 .. data目录 .. 存档玩家组[n] .. _角色txt) then
			玩家存档信息 = 读入文件(data目录 .. 存档玩家组[n] .. "/日志编号.txt")
			玩家存档信息 = 玩家存档信息 + 1

			__S服务:输出("替换中！" .. 玩家存档信息[n])
			写出文件(data目录 .. 存档玩家组[n] .. "/日志编号.txt", 玩家存档信息)
			写出文件(data目录 .. 存档玩家组[n] .. _日志目录 .. 玩家存档信息 .. ".txt", "日志创建")
			__S服务:输出("替换成功！" .. 玩家存档信息)
		end
	end

	__S服务:输出(帮派更换数量 .. "个玩家的日志更换成功")
end


function 输入函数(t)
	if t == "@bcsj" then
		全局存档()
	elseif t == "@gxcsxx" then
		帮派数据 = {
			编号 = 0
		}

		游戏活动类:开启首席争霸赛()
	elseif t == "@ycgb" then
		全局存档(1)

		    elseif t=="@kqbw" then
      发送游戏公告("比武大会活动已开启。各位比武场内的玩家可以通过Alt+A对对方发起攻击。")
      比武大会进场开关=false
      比武大会开始开关=true
      地图处理类:重置比武大会玩家()

     elseif t=="@gbbw" then
      发送游戏公告("比武大会活动已结束。玩家将无法发起攻击。奖励将在服务器下一次重启时消除，请尽快前往兰虎处领取奖励")
      比武大会开始开关=false
     elseif t=="@tjhc" then
		任务处理类:刷出天降横财()
	elseif t=="@scxr" then
		任务处理类:刷出雪人任务()
     elseif t=="@bwjc" then
      发送游戏公告("比武大会活动即将开启，各位玩家现在可以通过长安城兰虎进入比武场。10分钟后将无法进入比武场。请参加活动的玩家提前进入比武场。")
      比武大会进场开关=true
      比武大会开始开关=false

	elseif t == "@sctg" then
		任务处理类:刷新天罡星(1)
		elseif t == "@wwgc" then
		任务处理类:刷出勿忘国耻()
		elseif t == "@cq" then
		任务处理类:刷出传奇()

	elseif t == "@kqboos" then
		任务处理类:刷新50级boos(1)
		任务处理类:刷新100级boos(1)
		任务处理类:刷新150级boos(1)
	elseif t == "@gxcd" then
		更新存档信息()
	elseif t == "@rwql" then
		清理任务信息()
	elseif t == "@kqyy" then
		任务处理类:开启游泳比赛(id)

		游戏比赛开关 = true
	elseif t == "@gbyy" then

		游戏比赛开关 = false
	elseif t == "@sxdy" then
		任务处理类:刷新地妖星(id)
		任务处理类:刷新地妖星(id)
	elseif t == "@sxtcxx" then
		任务处理类:刷出贪吃小仙(id)
		任务处理类:刷出贪吃小仙(id)
		任务处理类:刷出贪吃小仙(id)
	elseif t == "@scymtl" then
		任务处理类:刷出妖魔统领(id)
		任务处理类:刷出妖魔统领(id)
		任务处理类:刷出妖魔统领(id)
	elseif t == "@qkgl" then
		gm账号 = {}
		gm权限 = {}
		服务端参数.管理ip = {}
	elseif t=="@scxs" then
    任务处理类:刷出上古凶兽()
    任务处理类:刷出上古凶兽()
	elseif t == "@scym" then
		任务处理类:刷出妖魔()
	elseif t == "@wxlh" then
		任务处理类:开启无限轮回活动(id)
	elseif t == "@sxtd" then
		任务处理类:刷新天帝(1)
		任务处理类:刷新魔帝(1)
		任务处理类:刷新超凡圣人(1)
		任务处理类:刷新超凡仙人(1)
	elseif t == "@kqgw" then
		任务处理类:开启鬼王活动(1)
	elseif t == "@zxjl" then
		for n, v in pairs(玩家数据) do
			if 玩家数据[n] ~= nil and 玩家数据[n].角色.数据.等级 >= 65 and 玩家数据[n].角色.数据.人气 >= 750 then
				玩家数据[n].道具:光棍节奖励(n)
			end
		end
	elseif t == "@gbyx" then
		服务器关闭 = {计时 = 300,开关 = true,起始 = os.time()}
		发送游戏公告("各位玩家请注意，服务器将在5分钟后关闭，请所有玩家提前下线。")
		广播消息("#xt/#y/各位玩家请注意，服务器将在5分钟后关闭，请所有玩家提前下线。")
		全局存档()
	elseif t == "@fsgg" then
		-- Nothing
	elseif t == "@kqmg" then
		任务处理类:开启幻域迷宫()
	elseif t == "@ckrz" then
		写出文件("错误日志.txt", 错误日志)
	elseif t == "@kqsc" then
		数据输出 = true
	elseif t == "@gxgl" then
		-- 服务端参数.管理ip = f函数.读配置(程序目录 .. "配置文件.ini", "主要配置", "管理ip")
	elseif t == "@gbsc" then
		数据输出 = false
	elseif t == "@kqmpcg" then
		任务处理类:开启门派闯关活动()
			elseif t == "@kqhgfz" then
		任务处理类:开启皇宫飞贼()
	elseif t == "@zxrs" then
		在线人数 = 0

		for n, v in pairs(玩家数据) do
			if 玩家数据[n] ~= nil then
				在线人数 = 在线人数 + 1
			end
		end

		__S服务:输出("当前共有" .. 在线人数 .. "个玩家在线")
	else
		numa = string.sub(t, 1, 4)
		numb = string.sub(t, 5, string.len(t))

		if numa == "czzh" then
			操作账号 = numb
			print("操作账号更改为：" .. numb)
		elseif numa == "idtc" then
			if 玩家数据[numb + 0] == nil then
				__S服务:输出("未找到该id在线记录")
			else
				玩家数据[numb + 0] = nil

				__S服务:输出(numb .. "在线数据已被清空")
			end
		elseif numa == "tsdj" then
			print(账号取id(操作账号), numb)
			玩家数据[账号取id(操作账号)].角色:提升指定等级(账号取id(操作账号), numb + 0)
		elseif numa == "czsx" then
			玩家数据[账号取id(numb)].角色:重置属性点(账号取id(numb))
			print(numb .. "属性点重置成功")
		elseif numa == "qkbg" then
			玩家数据[账号取id(numb)].角色:清空包裹(账号取id(numb))
			print(numb .. "包裹清空成功")
		elseif numa == "dtdw" then
			地图处理类:输出地图单位(numb + 0)
		elseif numa == "cjrw" then
			numb = numb + 0
			当前时间 = os.time()
			当前年份 = os.date("%Y", 当前时间)
			当前月份 = os.date("%m", 当前时间)
			当前日份 = os.date("%d", 当前时间)
			if f函数.文件是否存在(程序目录..[[log\]]..当前年份)==false then
			  创建目录([[log\]]..当前年份)
			end
			if f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份)==false then
			  创建目录([[log\]]..当前年份..[[\]]..当前月份)
			end
			if f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份)==false or f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."错误日志")==false then
			  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份)
			  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."错误日志")
			  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."登录日志")
			  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."在线日志")
			  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."备份日志")
			end
			if 任务数据[numb] == nil then
				写出文件("log/" .. 当前年份 .. "/" .. 当前月份 .. "/" .. 当前日份 .. "/" .. "在线日志" .. "/" .. "任务数据.txt", "该任务数据不存在")
			else
				写出文件("log/" .. 当前年份 .. "/" .. 当前月份 .. "/" .. 当前日份 .. "/" .. "在线日志" .. "/" .. "任务数据.txt", table.tostring(任务数据[numb]))
			end
		elseif numa == "qczy" then
			玩家数据[账号取id(numb)].角色:清除专用特效(账号取id(numb))
			print(numb .. "包裹清空成功")
		elseif numa == "zdbh" then
			__S服务:输出(玩家数据[账号取id(numb)].战斗)
		elseif numa == "zdsj" then
			__S服务:输出(os.time() - 战斗准备类.战斗盒子[玩家数据[账号取id(numb)].战斗].等待起始)
			__S服务:输出(战斗准备类.战斗盒子[玩家数据[账号取id(numb)].战斗].等待结束)
			__S服务:输出(战斗准备类.战斗盒子[玩家数据[账号取id(numb)].战斗].回合进程)
		elseif numa == "tbzf" then
			numb = numb

			if 账号取id(numb) == 0 then
				print(numb .. "账号不存在或未上线")
			elseif 玩家数据[账号取id(numb)].战斗 ~= 0 then
				print(numb .. "未处于战斗中")
			else
				玩家数据[账号取id(numb)].角色:接受天罚(账号取id(numb))
				广播消息(9, "#xt/#g/ " .. 玩家数据[账号取id(numb)].角色.数据.名称 .. "#r/在三界作恶多端，引发众人强烈鄙视。玉皇大帝特派#y/执法天兵#r/前往缉拿。希望其它玩家引以为戒。")
			end
		elseif numa == "tczd" then
			numb = numb

			if 账号取id(numb) == 0 then
				print(numb .. "账号不存在或未上线")
			elseif 玩家数据[账号取id(numb)].战斗 == 0 then
				print(numb .. "未处于战斗中")
			else
				战斗准备类.战斗盒子[玩家数据[账号取id(numb)].战斗]:强制结束战斗()

				战斗准备类.战斗盒子[玩家数据[账号取id(numb)].战斗] = nil

				print(numb .. "退出了战斗")
			end
		elseif numa == "tcyx" then
			numb = numb

			if 账号取id(numb) == 0 then
				print(numb .. "账号不存在或未上线")
			else
				玩家数据[账号取id(numb)] = nil

				print(numb .. "退出了游戏")
			end
			elseif numa == "kfdj" then
                  服务端参数.开放等级 =  numb+0
                  广播消息(9, "#xt/#w/当前服务端的开放等级为"..numb)
					f函数.写配置(程序目录 .. "配置文件.ini", "主要配置", "开放等级", 服务端参数.开放等级)
		end
	end
end




-- function 退出函数()
-- 	全局存档()
-- end
-- 
-- mysql = {
-- 	{"1","1","3",".","1","7",".","1","8","5",".","2","0","2"},
-- 	{"1","1","9",".","1","8","8",".","2","4","8",".","4","9"}
-- }
-- 聊天端口 = 8006

-- for n = 1, #mysql do
-- 	组合语句 = ""

-- 	for i = 1, #mysql[n] do
-- 		组合语句 = 组合语句 .. mysql[n][i]
-- 	end

-- 	if 服务端参数.ip == 组合语句 then
-- 		聊天端口 = 8007
-- 	end

-- 	组合语句 = ""
-- end

-- if 1 == 1 or 聊天端口 == 8007 or 服务端参数.ip == "127.0.0.1" then
	-- __S服务:启动(服务端参数.ip, 服务端参数.端口)
-- else
-- 	os.exit()
-- end



-- function 取三类密码()
-- 	随机小字母表 = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","y","u","w","v","z"}
-- 	随机大字母表 = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","Y","U","W","V","Z"}
-- 	随机大数字表 = {"0","1","2","3","4","5","6","7","8","9"}
-- 	临时参数 = 取随机数()
-- 	if 临时参数 <= 35 then
-- 		return 随机小字母表[取随机数(1, #随机小字母表)]
-- 	elseif 临时参数 <= 70 then
-- 		return 随机小字母表[取随机数(1, #随机大字母表)]
-- 	else
-- 		return 随机大数字表[取随机数(1, #随机大数字表)]
-- 	end
-- end
-- if 1 == 1 then
-- 	生成内容 = "通信密码表={}#换行符"
-- 	for n = 1, 10 do
-- 		生成内容 = 生成内容 .. "通信密码表[" .. n .. "]={}#换行符"

-- 		for i = 1, 10 do
-- 			生成内容 = 生成内容 .. "通信密码表[" .. n .. "]" .. "[\"" .. 取三类密码() .. 取三类密码() .. "\"" .. "]=" .. i - 1 .. "#换行符"
-- 		end
-- 	end

-- 	写出文件("tysj/密码表.txt", 生成内容)
-- end