local 召唤兽处理类 = class()

function 召唤兽处理类:初始化(id)
	self.数据 = {
		参战 = 0
	}
	self.玩家id = id
end

function 召唤兽处理类:更新(dt)
end

function 召唤兽处理类:加载数据(q, w, e)
	self.数据 = table.loadstring(q)

	if self.数据.参战 == nil then
		self.数据.参战 = 0
	end

	if self.数据.参战 ~= 0 then
		if self.数据[self.数据.参战].等级 > 玩家数据[self.玩家id].角色.数据.等级 + 10 then
			self.数据.参战 = 0
		elseif 玩家数据[self.玩家id].角色.数据.等级 < 召唤兽[self.数据[self.数据.参战].造型].参战等级 then
			self.数据.参战 = 0
		end
	end

	for n = 1, #self.数据 do
		self.总属性点 = 0
		self.总属性点 = self.数据[n].体质 + self.数据[n].力量 + self.数据[n].耐力 + self.数据[n].魔力 + self.数据[n].敏捷 + self.数据[n].潜能
		if self.数据[n].清空默认 == nil then
			self.数据[n].默认法术 = nil
			self.数据[n].清空默认 = true
		end

		if self.数据[n].神兽 and self.数据[n].寿命 <= 50 then
			self.数据[n].寿命 = 5000
		end
		if self.总属性点 > 110 + self.数据[n].等级 * 10 then
			-- Nothing
		elseif self.数据[n].禁止使用 then
			self.数据[n].禁止使用 = false
		end

		if self.数据[n].等级 > 玩家数据[self.玩家id].角色.数据.等级 + 10 and self.数据[n].宝宝 then
			self:降级处理(n, self.玩家id)
		end

		if self.数据[n].装备数据异常 == nil then
			self:刷新属性(n)
			self.数据[n].装备数据异常 = true
		end


	end
end

function 召唤兽处理类:飞升处理(id)
	for n = 1, #self.数据 do
		if self.数据[n] ~= nil and self.数据[n].等级 > 玩家数据[id].角色.数据.等级 + 10 then
			self:降级处理(n, id)
		end
	end
end

function 召唤兽处理类:降级处理(编号, id)
	self.临时等级 = self.数据[编号].等级
	self.数据[编号].等级 = 玩家数据[id].角色.数据.等级 + 10
	self.数据[编号].升级经验 = math.floor(升级经验[self.数据[编号].等级] * 0.1)
	self.数据[编号].当前经验 = 0

	玩家数据[id].角色:添加系统消息(id, "#h/您的召唤兽#g/" .. self.数据[编号].名称 .. "#h/的等级由原来的#r/" .. self.临时等级 .. "#h/级下降至#r/" .. self.数据[编号].等级 .. "#h/级。")
	self:重置属性点(编号)
end

function 召唤兽处理类:提级处理(编号, id)
	self.数据[编号].等级 = 玩家数据[id].角色.数据.等级 + 10
	self.数据[编号].升级经验 = math.floor(升级经验[self.数据[编号].等级] * 0.1)
	self.数据[编号].当前经验 = 0

	self:重置属性点(编号)
end

function 召唤兽处理类:提级处理1(编号, id, 等级)
	self.数据[编号].等级 = 等级
	self.数据[编号].升级经验 = math.floor(升级经验[self.数据[编号].等级] * 0.1)
	self.数据[编号].当前经验 = 0

	self:重置属性点(编号)
end

function 召唤兽处理类:重置属性点(编号)
	self.数据[编号].力量 = self.数据[编号].等级 + 10
	self.数据[编号].魔力 = self.数据[编号].等级 + 10
	self.数据[编号].体质 = self.数据[编号].等级 + 10
	self.数据[编号].敏捷 = self.数据[编号].等级 + 10
	self.数据[编号].耐力 = self.数据[编号].等级 + 10
	self.数据[编号].潜能 = self.数据[编号].等级 * 5 + self.数据[编号].最高灵性 * 2 + 50
	self:刷新属性(编号, 恢复)
end

function 召唤兽处理类:重置等级(编号)
	self.数据[编号].等级 = 0
	self.数据[编号].升级经验 = 10
	self:重置属性点(编号)
end

function 召唤兽处理类:获取指定数据(编号)
	return table.tostring(self.数据[编号])
end

function 召唤兽处理类:获取指定数据1(编号)
	return self.数据[编号]
end

function 召唤兽处理类:获取数据()
	return self.数据
end

function 召唤兽处理类:参战召唤兽(id, 编号)
	if self.数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这只召唤兽")
	elseif 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法进行此操作")

		return 0
	-- elseif self.数据[编号].等级 >= 玩家数据[id].角色.数据.等级 + 11 then
	-- 	发送数据(玩家数据[id].连接id, 7, "#y/你无法驾驭等级大于你10级的召唤兽")
	else
		if self.数据.参战 == 编号 then
			self.数据.参战 = 0

			发送数据(玩家数据[id].连接id, 2008, 编号)
		else
			self.数据.参战 = 编号

			发送数据(玩家数据[id].连接id, 2007, 编号)
		end

		发送数据(玩家数据[id].连接id, 2005, self:取头像数据())
	end
end

function 召唤兽处理类:放生召唤兽(id, 编号)

	if self.数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这只召唤兽")

		return 0
	elseif 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法进行此操作")

		return 0
	else

      玩家数据[id].召唤兽id=编号

        self.对话内容=[[ 第一次提示放生，第二次直接放生#R/]]..[[


    ]].."#R/ht|".."66"..[[/放生这只召唤兽

    #R/ht|0/取消


         ]]
        self.发送信息={编号=nil,对话=self.对话内容,名称="放生宠物"}
        发送数据(玩家数据[id].连接id,20,self.发送信息)


	end
end



function 召唤兽处理类:放生召唤兽1(id, 编号)
	if self.数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这只召唤兽")
		return 0
	elseif 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法进行此操作")
		return 0
	else
		self.数据.参战 = 0
		玩家数据[id].角色:添加消费日志(string.format("放生召唤兽:编号=%s,类型=%s,名称=%s,等级=%s,技能数=%s", 编号, self.数据[编号].造型, self.数据[编号].名称, self.数据[编号].等级, #self.数据[编号].技能))
		table.remove(self.数据, 编号)
		发送数据(玩家数据[id].连接id, 2006, self:获取数据())
		发送数据(玩家数据[id].连接id, 2005, self:取头像数据())
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽消失的无影无踪")
		确定放生= false
	end
end
function 召唤兽处理类:改名召唤兽(id, 序号, 名称)
	self.临时数据 = {
		序号 = 序号,
		名称 = 名称
	}

	if self.数据[self.临时数据.序号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这只召唤兽")
	elseif 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法进行此操作")

		return 0
	elseif string.len(self.临时数据.名称) > 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/名称长度太长，请再想个短一点的名称吧")
	else
		self.数据[self.临时数据.序号].名称 = self.临时数据.名称

		发送数据(玩家数据[id].连接id, 7, "#y/召唤兽改名成功")
		发送数据(玩家数据[id].连接id, 2006, self:获取数据())
	end
end

function 召唤兽处理类:加点召唤兽(id, 序号, 参数)
	self.临时数据 = {
		序号 = 序号,
		参数 = 参数
	}

	if self.数据[self.临时数据.序号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这只召唤兽")
	elseif 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法进行此操作")

		return 0
	elseif self.数据[self.临时数据.序号].潜能 <= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽没有那么多的可分配属性点")
	else
		self.数据[self.临时数据.序号][self.临时数据.参数] = self.数据[self.临时数据.序号][self.临时数据.参数] + 1
		self.数据[self.临时数据.序号].潜能 = self.数据[self.临时数据.序号].潜能 - 1

		发送数据(玩家数据[id].连接id, 7, "#y/召唤兽属性点分配成功")
		self:刷新属性(self.临时数据.序号)
		发送数据(玩家数据[id].连接id, 2005, self:取头像数据())
		发送数据(玩家数据[id].连接id, 2009, self:获取数据(编号))
	end
end

function 召唤兽处理类:创建召唤兽(造型,宝宝,变异,方式,等级,禁止)

  self.数据[#self.数据+1]={}
	self.数据[#self.数据]={
	造型=造型,名称=造型,参战等级 = 召唤兽[造型].参战等级,进阶 = false,灵性 = 0,特性 = "无",进阶属性 = {力量=0,敏捷=0,耐力=0,魔力=0,体质=0},特殊资质={攻资=0,防资=0,体资=0,法资=0,速资=0,躲资=0},最高灵性 = 0,最高灵性显示 =false,特性几率=0,
	五行=生成五行(),潜能=0,装备={},装备属性 = {速度 = 0,体质 = 0,魔力 = 0,气血 = 0,敏捷 = 0,耐力 = 0,魔法 = 0,灵力 = 0,伤害 = 0,力量 = 0,防御 = 0},附加被动技能 = {},追加技能 = {},饰品=false
 }


  if 方式==3 then
   self.数据[#self.数据].神兽=true
    end
  if 变异 or 宝宝 then
     self.数据[#self.数据].宝宝=true
     if 禁止==nil then
       等级=0
       end
  else
     self.数据[#self.数据].宝宝=false
   end
  self.数据[#self.数据].变异=变异
  -- if self.数据[#self.数据].神兽 then
  self.数据[#self.数据].内丹 = {总数=6,可用数量=6,}
  -- else
  -- self.数据[#self.数据].内丹 = {
  -- 总数=math.floor(self.数据[#self.数据].参战等级 / 35)+1,
  -- 可用数量 = math.floor(self.数据[#self.数据].参战等级 / 35)+1
 -- }
  -- end

  if 等级==nil then
  self.数据[#self.数据].等级=0
  else
  self.数据[#self.数据].等级=等级
  end
  self.数据[#self.数据].寿命=5000
  self.数据[#self.数据].忠诚=80
  self.数据[#self.数据].当前经验=0
  self.数据[#self.数据].升级经验=15
  self.数据[#self.数据].附加技能={}
  self.数据[#self.数据].元宵=30
  self.数据[#self.数据].珍兽经=20
  self.数据[#self.数据].套装=""
  self:创建属性点(#self.数据,方式)
  self:创建资质(#self.数据,方式)
  self:创建技能(#self.数据,方式)
  self:刷新属性(#self.数据)
  return #self.数据

  end

function 召唤兽处理类:创建扶贫(等级)
	local length = self:创建召唤兽("海毛虫",true,false,3,等级,false);
	local data = self.数据[length];
	data.特征 = "精准扶贫";
	data.潜能 = data.潜能 + 50;
	for i = 1,#全局变量.基础属性 do
		data[全局变量.基础属性[i]] = data[全局变量.基础属性[i]] - 10;
	end
	data.攻资=1200
 	data.防资=1000
	data.体资=3000
	data.法资=2000
	data.速资=1600
	data.躲资=1000
	data.成长=1.1
	data.技能[1]="高级必杀"
	data.技能[2]="高级连击"
 	data.技能[3]="高级吸血"
	data.技能[4]="高级隐身"
	data.技能[5]="苍鸾怒击"
	data.技能[6]="嗜血追击"
	self:刷新属性(length)
end


function 角色处理类:赠送召唤兽(id, 类型, 仙玉)
	if #玩家数据[id].召唤兽.数据 >= 5 then
		发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
	elseif self:扣除仙玉(仙玉,"赠送召唤兽") then
		发送数据(玩家数据[id].连接id, 7, "#y/你得到神兽" .. 类型)
		玩家数据[id].召唤兽:创建召唤兽(类型, true, false, 3)
	end
end

function 召唤兽处理类:取装备状态(编号)
	for n = 1, 3 do
		if self.数据[编号].装备[n] ~= nil then
			return true
		end
	end

	return false
end

function 召唤兽处理类:刷新装备属性new(编号, id)
	self.数据[编号].装备属性 = {速度 = 0,体质 = 0,魔力 = 0,气血 = 0,敏捷 = 0,耐力 = 0,魔法 = 0,灵力 = 0,伤害 = 0,力量 = 0,防御 = 0}
	self.数据[编号].道具数据 = {}
	for n = 1, 3 do
		if self.数据[编号].装备[n] ~= nil then
			self.数据[编号].道具数据[n] = 玩家数据[id].道具.数据[self.数据[编号].装备[n]]
			if self.数据[编号].道具数据[n].套装效果 ~= nil then
				self.套装名称 = self.数据[编号].道具数据[n].套装效果.名称

				if self.数据[编号].道具数据[n].套装效果.类型 == "追加法术" then
					if self.临时追加[self.套装名称] == nil then
						self.临时追加[self.套装名称] = 1
					else
						self.临时追加[self.套装名称] = self.临时追加[self.套装名称] + 1
					end
				elseif self.临时附加[self.套装名称] == nil then
					self.临时附加[self.套装名称] = 1
				else
					self.临时附加[self.套装名称] = self.临时附加[self.套装名称] + 1
				end
			end


			self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].主属性.名称] = self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].主属性.名称] + 玩家数据[id].道具.数据[self.数据[编号].装备[n]].主属性.数值

			if 玩家数据[id].道具.数据[self.数据[编号].装备[n]].附属性 ~= nil then
				self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].附属性.名称] = self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].附属性.名称] + 玩家数据[id].道具.数据[self.数据[编号].装备[n]].附属性.数值
			end

			if 玩家数据[id].道具.数据[self.数据[编号].装备[n]].单加 ~= nil then
				self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].单加.名称] = self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].单加.名称] + 玩家数据[id].道具.数据[self.数据[编号].装备[n]].单加.数值
			end

			if 玩家数据[id].道具.数据[self.数据[编号].装备[n]].双加 ~= nil then
				self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].双加.名称] = self.数据[编号].装备属性[玩家数据[id].道具.数据[self.数据[编号].装备[n]].双加.名称] + 玩家数据[id].道具.数据[self.数据[编号].装备[n]].双加.数值
			end
		else
			self.数据[编号].道具数据[n] = nil
		end
	end
end


function 召唤兽处理类:刷新装备属性(编号, id)
	self.数据[编号].追加技能 = {}
	self.数据[编号].附加技能 = {}
	self.临时追加 = {}
	self.临时附加 = {}
	local first = (not self.数据[编号].装备属性 or not self.数据[编号].道具数据)
	if not first then
		for n = 1, #全局变量.基础属性 do
			self.数据[编号][全局变量.基础属性[n]] = self.数据[编号][全局变量.基础属性[n]] - self.数据[编号].装备属性[全局变量.基础属性[n]]
		end
	end

	self:刷新装备属性new(编号, id)

	for n, v in pairs(self.临时追加) do
		if self.临时追加[n] >= 3 then

			self.数据[编号].追加技能[#self.数据[编号].追加技能+1] = n

		end
	end

	for n, v in pairs(self.临时附加) do
		if self.临时附加[n] >= 3 then
			self.数据[编号].附加技能[#self.数据[编号].附加技能+1] = n

		end
	end

	if not first then
		for n = 1, #全局变量.基础属性 do
			self.数据[编号][全局变量.基础属性[n]] = self.数据[编号][全局变量.基础属性[n]] + self.数据[编号].装备属性[全局变量.基础属性[n]]
		end
	end
	self:刷新属性(编号, id)
end

function 召唤兽处理类:刷新属性(编号,id)
if self.数据[编号].装备属性==nil then
   self.数据[编号].装备属性={速度=0,伤害=0,防御=0,气血=0,魔法=0,灵力=0,体质=0,力量=0,魔力=0,耐力=0,敏捷=0}
end

 if self.数据[编号].饰品 then
  self.数据[编号].特殊资质={
  攻资=math.floor(self.数据[编号].攻资*0.1),
  防资=math.floor(self.数据[编号].防资*0.1),
  体资=math.floor(self.数据[编号].体资*0.1),
  法资=math.floor(self.数据[编号].法资*0.1),
  速资=math.floor(self.数据[编号].速资*0.1),
  躲资=math.floor(self.数据[编号].躲资*0.1)}
 end

 self.临时等级=self.数据[编号].等级
 self.临时成长=self.数据[编号].成长

 self.数据[编号].伤害=math.floor(self.临时等级*(self.数据[编号].攻资+self.数据[编号].特殊资质.攻资)*(14+10*self.临时成长)/7500+self.临时成长*(self.数据[编号].力量+self.数据[编号].进阶属性.力量)+20+self.数据[编号].装备属性.伤害)
 self.数据[编号].灵力=math.floor(
 		self.临时等级 * (self.数据[编号].法资+self.数据[编号].特殊资质.法资) / 2000 +
 		self.临时成长 * (self.数据[编号].魔力+self.数据[编号].进阶属性.魔力) + 20 +
 	    self.数据[编号].装备属性.灵力 +
 	    (self.数据[编号].体质+self.数据[编号].进阶属性.体质)*0.3 +
 	    (self.数据[编号].力量+self.数据[编号].进阶属性.力量)*0.4 +
 	    (self.数据[编号].耐力+self.数据[编号].进阶属性.耐力)*0.2)
 self.数据[编号].防御=math.floor(self.临时等级*(self.数据[编号].防资+self.数据[编号].特殊资质.防资)*(9.4+19/3*self.临时成长)/7500+self.临时成长*(self.数据[编号].耐力+self.数据[编号].进阶属性.耐力)*4/3)+self.数据[编号].装备属性.防御
 self.数据[编号].速度=math.floor((self.数据[编号].敏捷+self.数据[编号].进阶属性.敏捷)*self.数据[编号].速资/1000)+self.数据[编号].装备属性.速度
 self.数据[编号].躲闪=math.floor((self.数据[编号].敏捷+self.数据[编号].进阶属性.敏捷)*self.数据[编号].躲资/1000)

 self.数据[编号].气血上限=math.floor(self.临时等级*(self.数据[编号].体资+self.数据[编号].特殊资质.体资)/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*6)+10+self.数据[编号].装备属性.气血
 self.数据[编号].最大气血=math.floor(self.临时等级*(self.数据[编号].体资+self.数据[编号].特殊资质.体资)/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*6)+10+self.数据[编号].装备属性.气血
 self.数据[编号].魔法上限=math.floor(self.临时等级*(self.数据[编号].法资+self.数据[编号].特殊资质.法资)/2000+self.临时成长*(self.数据[编号].魔力+self.数据[编号].进阶属性.魔力)*2.5)+self.数据[编号].装备属性.魔法

  for i=1,#self.数据[编号].内丹 do

  if self.数据[编号].内丹[i].技能=="迅敏" then
    self.数据[编号].伤害=self.数据[编号].内丹[i].等级*50+self.数据[编号].伤害
    self.数据[编号].速度=self.数据[编号].内丹[i].等级*20+self.数据[编号].速度
  end
  if self.数据[编号].内丹[i].技能=="静岳" then
    self.数据[编号].灵力=self.数据[编号].内丹[i].等级*32+self.数据[编号].灵力
    self.数据[编号].气血上限=self.数据[编号].内丹[i].等级*80+self.数据[编号].气血上限
  end
  if self.数据[编号].内丹[i].技能=="矫健" then
    self.数据[编号].气血上限=self.数据[编号].内丹[i].等级*120+self.数据[编号].气血上限
    self.数据[编号].速度=self.数据[编号].内丹[i].等级*15+self.数据[编号].速度
  end
  if self.数据[编号].内丹[i].技能=="玄武躯" then
    self.数据[编号].气血上限=self.数据[编号].内丹[i].等级*30+self.数据[编号].气血上限
  end
  if self.数据[编号].内丹[i].技能=="龙胄铠" then
    self.数据[编号].防御=self.数据[编号].内丹[i].等级*20+self.数据[编号].防御
  end

  end
  self.数据[编号].当前气血=self.数据[编号].气血上限
   self.数据[编号].当前魔法=self.数据[编号].魔法上限

   self.数据[编号].法防 = self.数据[编号].灵力
 -- if 恢复==nil then

 --    self.数据[编号].当前气血=self.数据[编号].气血上限
 --    self.数据[编号].气血上限=math.floor(self.临时等级*self.数据[编号].体资/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*6)+10+self.数据[编号].装备属性.气血
 --    self.数据[编号].最大气血=math.floor(self.临时等级*self.数据[编号].体资/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*6)+10+self.数据[编号].装备属性.气血
 --    self.数据[编号].魔法上限=math.floor(self.临时等级*self.数据[编号].法资/2000+self.临时成长*(self.数据[编号].魔力+self.数据[编号].进阶属性.魔力)*2.5)+self.数据[编号].装备属性.魔法
 --    self.数据[编号].当前魔法=self.数据[编号].魔法上限

 --   end
 --print(编号,恢复,self.玩家id)

	local data = self.数据[编号];
	if data.特征 == "精准扶贫" then
		local empty = false;
		for i = 1,3 do
			if data.装备[i] == nil or data.道具数据[i] == nil or data.道具数据[i].套装效果 == nil then
				empty = true;
				break;
			end
		end
		-- __S服务:输出(empty and '空装备' or "有装备")
		if  empty and data.等级 < 120  then
			-- __S服务:输出('有套')
 			data.追加技能 = {[1] = "剑荡四方"};
 		else
 			-- __S服务:输出('无套')
 			data.追加技能 = {};
 		end
 		if data.造型 == "海毛虫" and not data.进阶 and data.等级 < 120  then
 			data.伤害 = data.伤害 + data.等级 * 5 + 120
 		end
 	end
 end

function 召唤兽处理类:创建技能(编号, 方式)
	self.临时技能 = {}
	self.数据[编号].技能 = {}

	if 方式 == 1 or 方式 == 3 then
		for n = 1, #召唤兽[self.数据[编号].造型].技能 do
			self.临时技能[n] = 召唤兽[self.数据[编号].造型].技能[n]
		end
	end

	self.临时数量 = 0


	if 方式 == 1 then
		self.技能几率 = 60

		if 取随机数() >= 20 then
			self.临时数量 = 1
		end
	elseif 方式 == 3 then
		self.临时数量 = 4
		self.技能几率 = 100
	end

	if self.临时数量 ~= 0 then
		for n = 1, #self.临时技能 do
			if 取随机数() <= self.技能几率 then
				self.数据[编号].技能[#self.数据[编号].技能 + 1] = self.临时技能[n]
			end
		end
	end
end

function 召唤兽处理类:保留成长(成长)
	self.成长数量 = string.len(tostring(成长))

	if self.成长数量 > 5 then
		self.临时成长 = ""

		for n = 1, 5 do
			self.临时成长 = self.临时成长 .. string.sub(成长, n, n)
		end

		return tonumber(self.临时成长)
	else
		return 成长
	end
end

function 召唤兽处理类:创建资质(编号, 方式)
	if self.数据[编号] == nil then
		return 0
	end

	if 方式 == 1 then
		self.上限 = 1
		self.下限 = 0.75
	elseif 方式 == 3 then
		self.上限 = 1
		self.下限 = 1
	end

	for i = 1, #全局变量.资质标识 do
		if self.数据[编号] == nil then
			return 0
		end

		self.数据[编号][全局变量.资质标识[i]] = 取随机数(召唤兽[self.数据[编号].造型][全局变量.资质标识[i]] * self.下限, 召唤兽[self.数据[编号].造型][全局变量.资质标识[i]] * self.上限)
	end

	self.数据[编号].成长 = self:保留成长(召唤兽[self.数据[编号].造型].成长 * 取随机数(self.下限 * 100, self.上限 * 100) / 100)
end

function 召唤兽处理类:创建属性点(编号, 方式)
	self.基础属性 = 50

	for i = 1, #全局变量.基础属性 do
		self.数据[编号][全局变量.基础属性[i]] = 0
	end

	if self.数据[编号].宝宝 and (取随机数() <= 20 or 方式 == 3) then
		self.基础属性 = 100
	end

	if self.基础属性 == 100 then
		for i = 1, #全局变量.基础属性 do
			self.数据[编号][全局变量.基础属性[i]] = 20
		end
	else
		for n = 1, self.基础属性 do
			self.临时随机 = 取随机数(1, 5)
			self.数据[编号][全局变量.基础属性[self.临时随机]] = self.数据[编号][全局变量.基础属性[self.临时随机]] + 1
		end
	end

	if self.数据[编号].宝宝 == false then
		for n = 1, self.数据[编号].等级 * 5 do
			self.临时随机 = 取随机数(1, 5)
			self.数据[编号][全局变量.基础属性[self.临时随机]] = self.数据[编号][全局变量.基础属性[self.临时随机]] + 1
		end
	else
		self.数据[编号].潜能 = self.数据[编号].等级 * 5

		for i = 1, #全局变量.基础属性 do
			self.数据[编号][全局变量.基础属性[i]] = self.数据[编号][全局变量.基础属性[i]] + self.数据[编号].等级
		end
	end
end

function 召唤兽处理类:巫医治疗(id, 类型)
	if 类型 == 1 then
		self.临时参数 = self:取全体数值差(类型)
		self.扣除金钱 = math.floor(self.临时参数.气血 * 0.8 + self.临时参数.魔法 * 1.5)

		if 银子检查(id, self.扣除金钱) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本次操作需要扣除" .. self.扣除金钱 .. "两银子")
		else
			玩家数据[id].角色:扣除银子(id, self.扣除金钱, 7)
			self:恢复状态(1)
			发送数据(玩家数据[id].连接id, 7, "#y/您的所有召唤兽气血魔法已经恢复至最佳状态")
		end
	elseif 类型 == 2 then
		self.临时参数 = self:取全体数值差(类型)
		self.扣除金钱 = math.floor(self.临时参数 * 60)

		if 银子检查(id, self.扣除金钱) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本次操作需要扣除" .. self.扣除金钱 .. "两银子")
		else
			玩家数据[id].角色:扣除银子(id, self.扣除金钱, 7)
			self:恢复状态(2)
			发送数据(玩家数据[id].连接id, 7, "#y/您的所有召唤兽忠诚已经恢复至100")
		end
	elseif 类型 == 3 then
		self.临时参数 = self:取全体数值差(1)
		self.扣除金钱 = math.floor(self.临时参数.气血 * 0.8 + self.临时参数.魔法 * 1.5)
		self.临时参数 = self:取全体数值差(2)
		self.扣除金钱 = self.扣除金钱 + math.floor(self.临时参数 * 60)

		if 银子检查(id, self.扣除金钱) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本次操作需要扣除" .. self.扣除金钱 .. "两银子")
		else
			玩家数据[id].角色:扣除银子(id, self.扣除金钱, 7)
			self:恢复状态(3)
			发送数据(玩家数据[id].连接id, 7, "#y/您的所有召唤兽已经恢复至最佳状态")
		end
	end
end

function 召唤兽处理类:取全体数值差(类型)
	if 类型 == 1 then
		self.返回数据 = {
			魔法 = 0,
			气血 = 0
		}

		for n = 1, #self.数据 do
			self.返回数据.气血 = self.返回数据.气血 + self.数据[n].气血上限 - self.数据[n].当前气血
			self.返回数据.魔法 = self.返回数据.魔法 + self.数据[n].魔法上限 - self.数据[n].当前魔法
		end

		return self.返回数据
	elseif 类型 == 2 then
		self.返回数据 = 0

		for n = 1, #self.数据 do
			self.返回数据 = self.返回数据 + 100 - self.数据[n].忠诚
		end

		return self.返回数据
	end
end

function 召唤兽处理类:恢复状态(类型, id)
	if 类型 == 1 then
		if id ~= nil then
			self.数据[id].当前气血 = self.数据[id].气血上限
			self.数据[id].当前魔法 = self.数据[id].魔法上限
		else
			for n = 1, #self.数据 do
				self.数据[n].当前气血 = self.数据[n].气血上限
				self.数据[n].当前魔法 = self.数据[n].魔法上限
			end
		end
	elseif 类型 == 2 then
		if id ~= nil then
			self.数据[id].忠诚 = 100
		else
			for n = 1, #self.数据 do
				self.数据[n].忠诚 = 100
			end
		end
	elseif 类型 == 3 then
		if id ~= nil then
			self.数据[id].当前气血 = self.数据[id].气血上限
			self.数据[id].当前魔法 = self.数据[id].魔法上限
			self.数据[id].忠诚 = 100
		else
			for n = 1, #self.数据 do
				self.数据[n].当前气血 = self.数据[n].气血上限
				self.数据[n].当前魔法 = self.数据[n].魔法上限
				self.数据[n].忠诚 = 100
			end
		end
	end

	发送数据(玩家数据[self.玩家id].连接id, 2005, 玩家数据[self.玩家id].召唤兽:取头像数据())
end

function 召唤兽处理类:升级处理(玩家id, id)
	self.数据[id].当前经验 = self.数据[id].当前经验 - self.数据[id].升级经验
	self.数据[id].等级 = self.数据[id].等级 + 1
	self.数据[id].潜能 = self.数据[id].潜能 + 5
	self.数据[id].忠诚 = 100

	for i = 1, #全局变量.基础属性 do
		self.数据[id][全局变量.基础属性[i]] = self.数据[id][全局变量.基础属性[i]] + 1
	end

	self.数据[id].升级经验 = math.floor(升级经验[self.数据[id].等级] * 0.1)

	self:刷新属性(id)
	self:恢复状态(3, id, 玩家id)
	发送数据(玩家数据[玩家id].连接id, 9, "#xt/#y/ " .. self.数据[id].名称 .. "等级提升至#r/" .. self.数据[id].等级 .. "#y/级")
end

function 召唤兽处理类:连续升级处理(玩家id, id)
	local lvup = 0
	while true do
		if self.数据[id].等级 >= 玩家数据[玩家id].角色.数据.等级 + 10 or self.数据[id].当前经验 < self.数据[id].升级经验 then
			break
		end
		lvup = lvup + 1
		self.数据[id].等级 = self.数据[id].等级 + 1
		self.数据[id].当前经验 = self.数据[id].当前经验 - self.数据[id].升级经验

		self.数据[id].升级经验 = math.floor(升级经验[self.数据[id].等级] * 0.1)
	end
	if lvup == 0 then return end
	-- self.数据[id].等级 = self.数据[id].等级 + lvup
	self.数据[id].潜能 = self.数据[id].潜能 + lvup * 5
	self.数据[id].忠诚 = 100

	for i = 1, #全局变量.基础属性 do
		self.数据[id][全局变量.基础属性[i]] = self.数据[id][全局变量.基础属性[i]] + lvup
	end

	self:刷新属性(id)
	self:恢复状态(3, id, 玩家id)
	发送数据(玩家数据[玩家id].连接id, 9, "#xt/#y/ " .. self.数据[id].名称 .. "等级提升至#r/" .. self.数据[id].等级 .. "#y/级")
end

function 召唤兽处理类:添加经验(数额, 玩家id, id, 类型)
	if 类型 ~= 10 then
		self.临时数额 = 数额 * 服务端参数.经验获得率
	else
		self.临时数额 = 数额
	end

	if self.数据[id].等级 >= 玩家数据[玩家id].角色.数据.等级 + 10 then
		-- 发送数据(玩家数据[玩家id].连接id, 9, "#dq/#w/你的召唤兽等级已经高出人物10级，无法再获得经验")
	else
		self.数据[id].当前经验 = self.数据[id].当前经验 + self.临时数额

		发送数据(玩家数据[玩家id].连接id, 9, "#dq/#w/ " .. self.数据[id].名称 .. "获得" .. self.临时数额 .. "点经验")

		self:连续升级处理(玩家id, id)

	end

	发送数据(玩家数据[玩家id].连接id, 2005, 玩家数据[玩家id].召唤兽:取头像数据())
end

function 召唤兽处理类:存档(id)
	local temp = {}
	for n = 1, #self.数据 do
		temp[n] = {}
		for k, v in pairs(self.数据[n]) do
			if k == '道具数据' then
			elseif k == '装备属性' then
			else 
				temp[n][k] = v
			end
		end
	end
	temp.参战 = self.数据.参战 or 0
	写出文件(data目录 .. 玩家数据[id].账号 .. _召唤兽txt, table.tostring(temp))
end

function 召唤兽处理类:取头像数据()
	if self.数据.参战 == 0 then
		return 0
	elseif self.数据[self.数据.参战] == nil then
		return 0
	end

	self.发送信息 = {
		造型 = self.数据[self.数据.参战].造型,
		当前气血 = self.数据[self.数据.参战].当前气血,
		气血上限 = self.数据[self.数据.参战].气血上限,
		当前魔法 = self.数据[self.数据.参战].当前魔法,
		魔法上限 = self.数据[self.数据.参战].魔法上限,
		当前经验 = self.数据[self.数据.参战].当前经验,
		升级经验 = self.数据[self.数据.参战].升级经验
	}

	return self.发送信息
end


return 召唤兽处理类
