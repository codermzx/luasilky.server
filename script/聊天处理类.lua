local 聊天处理类 = class()

function 聊天处理类:初始化()
end

-- function 聊天处理类:数据处理1(id, 序号, 内容)
-- 	if 序号 == 7001 then
-- 		self:聊天频道发言处理(id, table.loadstring(内容[2]))
-- 	end
-- end

function 聊天处理类:数据处理(内容)
	self.临时数据 = 内容 --table.loadstring(内容)
	local id = self.临时数据.数字id

	if self.临时数据.文本 == "管理模式" and (玩家数据[id].管理 == "1" or 玩家数据[id].管理 == "2") then
		for n = 1, #服务端参数.管理ip do
			if 服务端参数.管理ip[n] == 玩家数据[id].角色.玩家ip then
				发送数据(玩家数据[id].连接id, 7, "#y/你已经开启过管理模式了")
				玩家数据[id].管理模式 = true
				return 0
			end
		end

		服务端参数.管理ip[#服务端参数.管理ip + 1] = 玩家数据[id].角色.玩家ip

		发送数据(玩家数据[id].连接id, 7, "#y/你开启了管理模式，权限为#r/" .. 玩家数据[id].管理 .. "#y/级")
		-- 发送数据(玩家数据[id].连接id, 7, "#y/你现在可以使用管理工具进行操作了!")

		玩家数据[id].管理模式 = true
		return 0
	end

	if (玩家数据[id].管理 == "1" or 玩家数据[id].管理 == "2") and 玩家数据[id].管理模式 == true then
		if self.临时数据.文本 == "退出管理" then
			玩家数据[id].管理模式 = false
			玩家数据[id].游戏管理 = "0"

			发送数据(玩家数据[id].连接id, 7, "#y/管理模式已经关闭")

			return 0
		end

		if self.临时数据.文本 == "cxds" then
			发送数据(玩家数据[id].连接id, 7, "#y/您账号可用点数为#r/" .. f函数.读配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数") .. "#y/点")

			return 0
		elseif self.临时数据.文本 == "在线人数" then
			在线人数 = 0

			for n, v in pairs(玩家数据) do
				if 玩家数据[n] ~= nil then
					在线人数 = 在线人数 + 1
				end
			end

			发送数据(玩家数据[id].连接id, 7, "#y/当前共有#r/" .. 在线人数 .. "#y/个玩家在线")

			return 0
		end

		if string.find(self.临时数据.文本, "@") == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/错误的游戏指令！")

			return 0
		else
			self.指令数据 = 分割文本(self.临时数据.文本, "@")

			if self.指令数据[1] == "jcrz11" then
				if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				end

				f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "qq", self.指令数据[3])
				发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "已经取消了认证状态")

				return 0
			elseif self.指令数据[1] == "cxxy" then
				if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				end

				发送数据(玩家数据[id].连接id, 9, self.指令数据[2] .. "账号信息如下：")
				发送数据(玩家数据[id].连接id, 9, " 密码：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "密码"))
				发送数据(玩家数据[id].连接id, 9, " 安全码：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "安全码"))
				发送数据(玩家数据[id].连接id, 9, " 仙玉：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉"))
				发送数据(玩家数据[id].连接id, 9, " 绑定qq：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "qq"))
				发送数据(玩家数据[id].连接id, 9, " 注册ip：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "ip"))
				发送数据(玩家数据[id].连接id, 9, " 注册时间：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "注册时间"))

				return 0
			elseif self.指令数据[1] == "zszq11" then

				if 账号取id(self.指令数据[2]) == 0 or 玩家数据[账号取id(self.指令数据[2])] == nil then
					发送数据(玩家数据[id].连接id, 7, "#y/赠送坐骑需要对方在线")

					return 0
				end

				玩家数据[账号取id(self.指令数据[2])].角色.数据.备用坐骑 = self.指令数据[3]

				发送数据(玩家数据[id].连接id, 7, "#y/赠送坐骑成功")
				发送数据(玩家数据[账号取id(self.指令数据[2])].连接id, 7, "#y/你获得了新坐骑，按下F2后可骑乘该坐骑")

				return 0
			elseif self.指令数据[1] == "fsgg11" then
				发送游戏公告(self.指令数据[2])
			elseif self.指令数据[1] == "fsgb11" then
				广播消息("#xt/#w/" .. self.指令数据[2])
			elseif self.指令数据[1] == "zjjy11" then
				self.临时账号 = self.指令数据[2]

				if 目录是否存在(data目录 .. self.临时账号) == "0" or f函数.文件是否存在(data目录 .. self.临时账号 .. _角色txt) == false then
					发送数据(id, 12, "要操作的账号不存在或没有建立角色")

					return 0
				else
					self.临时id = 账号取id(self.临时账号)

					if 玩家数据[self.临时id] == nil then
						发送数据(id, 12, "此操作必须玩家上线后才可进行")

						return 0
					else
						玩家数据[self.临时id].角色:添加经验(self.指令数据[3], self.临时id, 27)
						发送数据(玩家数据[id].连接id, 7, "#y/添加经验成功")
						发送数据(玩家数据[self.临时id].连接id, 7, "#y/您充值的#r/" .. self.指令数据[3] .. "#y/添加经验成功")
					end
				end
			elseif self.指令数据[1] == "xgmm11" then
				if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				end

				f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "密码", self.指令数据[3])
				发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "密码修改成功")

				return 0
			elseif self.指令数据[1] == "jfzh" then
				if f函数.文件是否存在(data目录 .. self.指令数据[2] .. _角色txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				end

				f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "封禁", "0")
				发送数据(玩家数据[id].连接id, 7, "#y/解封账号成功")

				return 0
			elseif self.指令数据[1] == "fjzh11" then
				f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "封禁", "1")
				发送数据(玩家数据[id].连接id, 7, "#y/封禁账号成功")

				if 账号取id(self.指令数据[2]) == 0 then
					发送数据(玩家数据[id].连接id, 7, "#y/账号不存在或未上线")

					return 0
				else
					网络处理类:强制断开1(玩家数据[账号取id(self.指令数据[2])].连接id, "你的账号已经被管理员封禁，你已经被强制断开连接。")
					登录处理类:玩家退出(账号取id(self.指令数据[2]))
					发送数据(玩家数据[id].连接id, 7, "#y/该账号已被强制下线")

					return 0
				end
			elseif self.指令数据[1] == "zjyz11" then
				self.临时账号 = self.指令数据[2]

				if 目录是否存在(data目录 .. self.临时账号) == "0" or f函数.文件是否存在(data目录 .. self.临时账号 .. _角色txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/要充值的账号不存在或没有建立角色")

					return 0
				else
					self.临时id = 账号取id(self.临时账号)

					if 玩家数据[self.临时id] == nil then
						发送数据(玩家数据[id].连接id, 7, "#y/账号不存在或未上线")

						return 0
					end

					self.充值数额 = (self.指令数据[3] + 0) * 10000
					self.扣除点数 = self.指令数据[3] + 0
					self.可用点数 = f函数.读配置(程序目录 .. data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数") + 0

					if self.可用点数 < self.扣除点数 then
						发送数据(玩家数据[id].连接id, 7, "#y/您的点数不足")

						return 0
					end

					self.可用点数 = self.可用点数 - self.扣除点数
					self.临时仙玉 = f函数.读配置(程序目录 .. data目录 .. self.临时账号.. _账号txt, "账号信息", "银子礼包")

					if self.临时仙玉 == "" or self.临时仙玉 == "空" then
						self.临时仙玉 = 0
					end

					self.临时仙玉 = self.临时仙玉 + self.充值数额

					f函数.写配置(程序目录 .. data目录 .. self.临时账号.. _账号txt, "账号信息", "银子礼包", self.临时仙玉)
					f函数.写配置(程序目录 .. data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数", self.可用点数)

					self.临时日志 = 读入文件(data目录 .. self.临时账号 .. _充值记录txt)
					self.临时日志 = self.临时日志 .. "#换行符银子充值：" .. 运行时间 .. "充值" .. self.充值数额 .. "点银子，操作账号：" .. 玩家数据[id].账号

					写出文件(data目录 .. self.临时账号 .. _充值记录txt, self.临时日志)

					self.临时日志 = 读入文件(data目录 .. 玩家数据[id].账号 .. _充值记录txt)
					self.临时日志 = self.临时日志 .. "#换行符点数消耗：" .. 运行时间 .. "为" .. self.临时账号 .. "充值" .. self.充值数额 .. "点银子，剩余点数" .. self.可用点数 .. "操作账号：" .. 玩家数据[id].账号

					写出文件(data目录 .. 玩家数据[id].账号 .. _充值记录txt, self.临时日志)

					self.临时id = 账号取id(self.临时账号)

					if 玩家数据[self.临时id] ~= nil then
						发送数据(玩家数据[self.临时id].连接id, 7, "#y/您有可领取的银子，请前往建业推广员处领取")
					end

					发送数据(玩家数据[id].连接id, 7, 运行时间 .. ":" .. self.临时账号 .. "充值" .. self.充值数额 .. "点银子成功" .. "*-*" .. self.可用点数 .. "")
				end
			elseif self.指令数据[1] == "tczd11" then
				if 账号取id(self.指令数据[2]) == 0 then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				elseif 玩家数据[账号取id(self.指令数据[2])].战斗 == 0 then
					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "未处于战斗状态中")

					return 0
				else
					战斗准备类.战斗盒子[玩家数据[账号取id(self.指令数据[2])].战斗]:强制结束战斗()

					战斗准备类.战斗盒子[玩家数据[账号取id(self.指令数据[2])].战斗] = nil

					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "退出了战斗，请重进游戏")

					return 0
				end
			elseif self.指令数据[1] == "cjzh11" then
				登录处理类:创建玩家账号(self.指令数据[2], self.指令数据[3], id, 玩家数据[id].角色.数据.离线ip)
				发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "账号创建成功")

				return 0
			elseif self.指令数据[1] == "ghzx" then
				if 角色武器类型[self.指令数据[2]].主武器 == nil then
					发送数据(玩家数据[id].连接id, 7, "#y/这种造型无法进行转换")

					return 0
				elseif 玩家数据[id].角色.数据.装备数据[21] ~= nil then
					发送数据(玩家数据[id].连接id, 7, "#y/请先卸下所佩戴的武器")

					return 0
				end

				玩家数据[id].角色.数据.造型 = self.指令数据[2]

				网络处理类:强制断开1(玩家数据[id].连接id, "你的角色造型已经更换，请重新登录。")
			elseif self.指令数据[1] == "zbhq11" then
				玩家数据[id].装备:取150级装备礼包(id)
			elseif self.指令数据[1] == "zsbb11" then
				self.临时账号 = self.指令数据[2]

				if 目录是否存在(data目录 .. self.临时账号) == "0" or f函数.文件是否存在(data目录 .. self.临时账号 .. _角色txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/要操作的账号不存在或没有建立角色")

					return 0
				else
					self.临时id = 账号取id(self.临时账号)

					if 玩家数据[self.临时id] == nil then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号当前未上线，无法执行道具操作")

						return 0
					end

					玩家数据[self.临时id].角色.数据.充值赠送.bb = 玩家数据[self.临时id].角色.数据.充值赠送.bb - 1

					玩家数据[self.临时id].角色:购买神兽1(self.临时id, self.指令数据[3], 0)
					发送数据(玩家数据[id].连接id, 7, "#y/赠送" .. self.指令数据[3])
					发送数据(玩家数据[self.临时id].连接id, 7, "赠送" .. self.指令数据[3] .. "成功")
					玩家数据[self.临时id].角色:添加消费日志("获得充值赠送的" .. self.指令数据[3])
				end
			elseif self.指令数据[1] == "zjxy" then
				self.临时账号 = self.指令数据[2]
				self.可用点数 = f函数.读配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数") + 0
				self.指令数据[3] = self.指令数据[3] + 0
				self.消除点数 = (self.指令数据[3] + 0) / 10

				if self.可用点数 < self.消除点数 then
					发送数据(玩家数据[id].连接id, 7, "#y/你的可用点数不足，请联系管理员充值")

					return 0
				elseif f函数.文件是否存在(data目录 .. self.指令数据[2] .. _角色txt) == false then
					发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

					return 0
				elseif self.指令数据[3] < 0 then
					发送数据(玩家数据[id].连接id, 7, "#y/添加的仙玉不允许为负数")

					return 0
				end

				self.可用点数 = self.可用点数 - self.消除点数

				发送数据(玩家数据[id].连接id, 7, "#y/您账号可用点数为#r/" .. self.可用点数 .. "#y/点")

				self.临时仙玉 = f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉") + 0

				-- if self.指令数据[3] >= 5000 and self.指令数据[3] < 10000 then
				-- 	self.指令数据[3] = math.floor(self.指令数据[3] * 1.2)
				-- elseif self.指令数据[3] >= 10000 then
				-- 	self.指令数据[3] = math.floor(self.指令数据[3] * 1.5)
				-- end

				self.临时仙玉 = self.临时仙玉 + self.指令数据[3]

				f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉", self.临时仙玉)
				f函数.写配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数", self.可用点数)
				发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "增加了#r/" .. self.指令数据[3] .. "#y/点仙玉")
				玩家数据[id].角色:添加消费日志(self.指令数据[2] .. "充值仙玉" .. self.指令数据[3] .. "点")

				self.临时id = 账号取id(self.临时账号)
				self.临时仙玉 = self.临时仙玉

				if 玩家数据[self.临时id] ~= nil then
					玩家数据[self.临时id].仙玉 = self.临时仙玉

					发送数据(玩家数据[self.临时id].连接id, 7, "#y/您充值的#r/" .. self.指令数据[3] .. "#y/点仙玉已经到账")
				end

			elseif self.指令数据[1] == "jyhdl" then
				服务端参数.经验获得率 = self.指令数据[2] + 0
				f函数.写配置(程序目录 .. "配置文件.ini", "主要配置", "经验", 服务端参数.经验获得率)
				发送数据(玩家数据[id].连接id, 7, "修改成功")
				广播消息("#xt/#y/游戏经验获得率已调整为#r/" .. self.指令数据[2] .. "#y/倍")
			elseif self.指令数据[1] == "yzhdl" then
				服务端参数.银子获得率 = self.指令数据[2] + 0
				f函数.写配置(程序目录 .. "配置文件.ini", "主要配置", "银子", 服务端参数.银子获得率)
				发送数据(玩家数据[id].连接id, 7, "修改成功")
				广播消息("#xt/#y/游戏银子获得率已调整为#r/" .. self.指令数据[2] .. "#y/倍")
			elseif self.指令数据[1] == "gbfwq" then
				服务器关闭 = {计时 = 300,开关 = true,起始 = os.time()}
				发送游戏公告("各位玩家请注意，服务器将在5分钟后关闭，请所有玩家提前下线。")
				广播消息("#xt/#y/各位玩家请注意，服务器将在5分钟后关闭，请所有玩家提前下线。")
			end

			return 0
		end
	end

	if os.time() < 玩家数据[id].角色.数据.出生日期 + 600 then
		发送数据(玩家数据[id].连接id, 7, "#y/角色建立后的10分钟内不可使用此功能")

		return 0
	end

	if 聊天监控 ~= nil then
		发送数据(聊天监控, 10, "[" .. 运行时间 .. "]" .. "频道[" .. self.临时数据.参数 .. "]" .. "[" .. 玩家数据[self.临时数据.数字id].账号 .. "]" .. "[" .. 玩家数据[self.临时数据.数字id].角色.数据.名称 .. "]" .. self.临时数据.文本)
	end

	if bb数据列表[id] ~= nil then
		self.新文本 = ""

		for n = 1, #bb数据列表[id] do
			self.是否替换 = true

			if string.find(self.临时数据.文本, bb数据列表[id][n].内容) ~= nil then
				self.战斗参数 = 分割文本(self.临时数据.文本, bb数据列表[id][n].内容)

				for i = 1, #self.战斗参数 do
					if i ~= #self.战斗参数 then
						self.新文本 = self.新文本 .. "#g/ht|" .. bb数据列表[id][n].内容 .. "/" .. "[" .. bb数据列表[id][n].名称 .. "]#w/" .. self.战斗参数[i]
					else
						self.新文本 = self.新文本 .. self.战斗参数[i]
					end
				end
			end
		end

		self.临时数据.文本 = self.新文本
		bb数据列表[id] = {}
	end

	if self.临时数据.参数 == 1 then
		if string.find(self.临时数据.文本, "pk") ~= nil and 1 == 1 then
			self.战斗参数 = 分割文本(self.临时数据.文本, "pk")

			if self.战斗参数[2] ~= nil and tonumber(self.战斗参数[2]) then
				self.战斗参数 = self.战斗参数[2] + 0

				if 玩家数据[self.战斗参数] == nil then
					发送数据(玩家数据[id].连接id, 7, "#y/这个玩家不存在或没在线")

					return 0
				elseif 玩家数据[self.战斗参数].摆摊 ~= nil then
					发送数据(玩家数据[id].连接id, 7, "#y/对方正处于摆摊状态，无法发起战斗")

					return 0
				elseif 玩家数据[id].角色.数据.pk开关 == false then
					发送数据(玩家数据[id].连接id, 7, "#y/请先按F4打开pk开关")

					return 0
				elseif 玩家数据[self.战斗参数].角色.数据.pk开关 then
					if 玩家数据[self.战斗参数].战斗 ~= 0 and 玩家数据[self.战斗参数].观战模式 == false then
						发送数据(玩家数据[id].连接id, 7, "#y/对方正在战斗中，请稍后再试")

						return 0
					elseif 玩家数据[self.战斗参数].地图 ~= 玩家数据[id].地图 then
						发送数据(玩家数据[id].连接id, 7, "#y/你与对方不处于同一地图内，无法发动攻击")

						return 0
					else
						if 玩家数据[id].地图 == 1001 or 玩家数据[id].地图 == 5131 or 玩家数据[id].地图 == 1092 then
							发送数据(玩家数据[id].连接id, 7, "#y/本地图不允许发起pk战斗")

							return 0
						end

						if 玩家数据[self.战斗参数].战斗 ~= 0 and 玩家数据[self.战斗参数].观战模式 then
							战斗准备类.战斗盒子[玩家数据[self.战斗参数].战斗]:退出观战(self.战斗参数)

							玩家数据[self.战斗参数].战斗 = 0
							玩家数据[self.战斗参数].观战模式 = false
						end

						玩家数据[id].战斗对象 = self.战斗参数

						广播消息(9, "#xt/#y/生死看淡,不服就干.#g/" .. 玩家数据[id].角色.数据.名称 .. "(" .. 玩家数据[id].数字id .. "）" .. "#y/对#g/" .. 玩家数据[玩家数据[id].战斗对象].角色.数据.名称 .. "(" .. 玩家数据[玩家数据[id].战斗对象].数字id .. ")#y/发起了pk战斗")
						战斗准备类:创建战斗(id, 200003, self.战斗参数, 0)
						玩家数据[玩家数据[玩家数据[id].战斗对象].数字id].角色:添加系统消息(玩家数据[玩家数据[id].战斗对象].数字id, "#h/" .. 玩家数据[id].角色.数据.名称 .. "[" .. 玩家数据[id].数字id .. "]对你发起了pk战斗。")

						return 0
					end
				elseif os.time() < 玩家数据[self.战斗参数].角色.数据.pk保护 then
					发送数据(玩家数据[id].连接id, 7, "#y/对方正处于保护期，无法对其发动攻击")

					return 0
				else
					玩家数据[id].战斗对象 = self.战斗参数
					self.对话内容 = " 你将花费200点人气对#Y/" .. 玩家数据[self.战斗参数].角色.数据.等级 .. "#W/级的#G/" .. 玩家数据[self.战斗参数].角色.数据.名称 .. [[
#W/发起战斗，若对方等级低于或高于你20级将额外扣除100点人气。请确认是否要发起战斗：

  #R/ht|pkzd65/切入战斗
  #R/ht|0/我再考虑会


     ]]
					self.发送信息 = {
						编号 = Q_人物头像[玩家数据[id].角色.数据.造型],
						对话 = self.对话内容,
						名称 = 玩家数据[id].角色.数据.名称
					}

					发送数据(玩家数据[id].连接id,20,self.发送信息)
					return 0
				end
			end
		end

		地图处理类:发送当前消息(self.临时数据.数字id, self.临时数据.文本)
		发送数据(玩家数据[self.临时数据.数字id].连接id, 1014, {
			消息 = self.临时数据.文本,
			id = 玩家数据[self.临时数据.数字id].数字id
		})
	elseif self.临时数据.参数 == 2 then
		if 玩家数据[self.临时数据.数字id].队伍 == 0 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/没有队伍的玩家不能在队伍频道发言")

			return 0
		end

		广播队伍消息(self.临时数据.数字id, 21, "#dw/#w/[" .. 玩家数据[self.临时数据.数字id].角色.数据.名称 .. "]#w/" .. self.临时数据.文本)
		发送数据(玩家数据[self.临时数据.数字id].连接id, 1014, {
			消息 = self.临时数据.文本,
			id = 玩家数据[self.临时数据.数字id].数字id
		})

		for n = 1, #队伍数据[玩家数据[self.临时数据.数字id].队伍].队员数据 do
			if 队伍数据[玩家数据[self.临时数据.数字id].队伍].队员数据[n] ~= self.临时数据.数字id then
				发送数据(玩家数据[队伍数据[玩家数据[self.临时数据.数字id].队伍].队员数据[n]].连接id, 1015, {
					消息 = self.临时数据.文本,
					id = 玩家数据[self.临时数据.数字id].数字id
				})
			end
		end
	elseif self.临时数据.参数 == 3 then
		if os.time() - 玩家数据[self.临时数据.数字id].世界频道 < 5 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/5秒才可在世界频道发言一次")

			return 0
		elseif 玩家数据[self.临时数据.数字id].角色.数据.等级 < 65 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/等级达到65级的玩家才可在世界频道发言")

			return 0
		elseif 银子检查(self.临时数据.数字id, 2000) == false then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/此频道发言需要消耗2000两银子")

			return 0
		end

		玩家数据[self.临时数据.数字id].世界频道 = os.time()

		玩家数据[self.临时数据.数字id].角色:扣除银子(self.临时数据.数字id, 2000, 22)

		if 三界书院.开关 and 三界书院.答案 == self.临时数据.文本 then
			self.名单重复 = false

			for n = 1, #三界书院.名单 do
				if 三界书院.名单[n].id == self.临时数据.数字id then
					self.名单重复 = true
				end
			end

			if self.名单重复 == false then
				三界书院.名单[#三界书院.名单 + 1] = {
					id = self.临时数据.数字id,
					名称 = 玩家数据[self.临时数据.数字id].角色.数据.名称,
					用时 = os.time() - 三界书院.起始
				}
			end
		end

		广播消息("#sj/#w/[" .. 玩家数据[self.临时数据.数字id].角色.数据.名称 .. "]#w/" .. self.临时数据.文本)
	elseif self.临时数据.参数 == 4 then
		if os.time() - 玩家数据[self.临时数据.数字id].门派频道 < 10 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/10秒才可在门派频道发言一次")

			return 0
		end

		if 玩家数据[self.临时数据.数字id].角色.数据.门派 == "" or 玩家数据[self.临时数据.数字id].角色.数据.门派 == "无" then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/无门派人士不能在门派频道发言")

			return 0
		end

		self.门派名称 = 玩家数据[self.临时数据.数字id].角色.数据.门派

		广播门派消息(self.门派名称, "#" .. 门派代号[self.门派名称] .. "/#w/[#w/" .. 玩家数据[self.临时数据.数字id].角色.数据.名称 .. "]#w/" .. self.临时数据.文本)
	elseif self.临时数据.参数 == 5 then
		if os.time() - 玩家数据[self.临时数据.数字id].传闻频道 < 60 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/60秒才可在传闻频道发言一次")

			return 0
		elseif 玩家数据[self.临时数据.数字id].角色.数据.等级 < 65 then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/等级达到65级的玩家才可在世界频道发言")

			return 0
		elseif 银子检查(self.临时数据.数字id, 10000) == false then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/此频道发言需要消耗10000两银子")

			return 0
		end

		玩家数据[self.临时数据.数字id].角色:扣除银子(self.临时数据.数字id, 10000, 22)

		玩家数据[self.临时数据.数字id].传闻频道 = os.time()
		self.临时名称 = 玩家数据[self.临时数据.数字id].角色.数据.名称

		if 取随机数() <= 50 then
			self.临时名称 = "某人"
		end

		广播消息("#cw/#w/[" .. self.临时名称 .. "]#w/" .. self.临时数据.文本)
	elseif self.临时数据.参数 == 6 then
		if 玩家数据[self.临时数据.数字id].角色.数据.帮派 == nil or 帮派数据[玩家数据[self.临时数据.数字id].角色.数据.帮派] == nil then
			发送数据(玩家数据[self.临时数据.数字id].连接id, 7, "#y/请先加入一个帮派")

			return 0
		end

		广播帮派消息(玩家数据[self.临时数据.数字id].角色.数据.帮派, "#bp/#w/[#w/" .. 玩家数据[self.临时数据.数字id].角色.数据.名称 .. "]#w/" .. self.临时数据.文本)
	end
end

function 聊天处理类:创建账号(数据, id)
	if 目录是否存在(data目录 .. 数据[1]) ~= "0" then
		发送数据(玩家数据[id].连接id, 7, "#y/账号" .. 数据[1] .. "已经存在，无法创建")

		return 0
	elseif 创建目录(data目录 .. 数据[1]) == "1" then
		写出文件(data目录 .. 数据[1].. _账号txt, 符号[1] .. "账号信息" .. 符号[2])
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "密码", 数据[2])
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "安全码", 数据[3])
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "仙玉", "0")
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "点数", "0")
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "管理", "0")
		f函数.写配置(data目录 .. 数据[1] .. "/" .. "账号.txt", "账号信息", "创建员", 玩家数据[id].账号)
		写出文件(data目录 .. 数据[1] .. "/操作纪录.txt", "账号创建#换行符")
		发送数据(玩家数据[id].连接id, 7, "#y/账号" .. 数据[1] .. "账号创建成功")

		return 0
	else
		发送数据(玩家数据[id].连接id, 7, "#y/账号" .. 数据[1] .. "账号创建失败，请联系管理员")

		return 0
	end
end

function 聊天处理类:增加仙玉(数据, id)
end

function 聊天处理类:聊天频道发言处理(id, 内容)

	if 玩家数据[id].认证 == "0" and 玩家数据[id].游戏管理 ~= 1 then
		发送数据(玩家数据[id].连接id, 7, "#y/认证模式下无法使用聊天功能，请联系管理员解除")

		return 0
	end

	if 玩家数据[id].游戏管理 == 1 then
		if 玩家数据[id].管理模式 == false and 内容.内容 == "kqgl" then
			玩家数据[id].管理模式 = true

			发送数据(玩家数据[id].连接id, 7, "#y/管理模式开启成功")
			发送数据(玩家数据[id].连接id, 7, "#y/处于管理模式下无法在聊天频道内发言")

			return 0
		elseif 玩家数据[id].管理模式 then
			if 内容.内容 == "cxds" then
				发送数据(玩家数据[id].连接id, 7, "#y/您账号可用点数为#r/" .. f函数.读配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数") .. "#y/点")

				return 0
			elseif 内容.内容 == "gbgl" then
				玩家数据[id].管理模式 = false

				发送数据(玩家数据[id].连接id, 7, "#y/管理模式已经关闭")

				return 0
			elseif 内容.内容 == "zxrs" then
				-- 输入函数("@zxlj")
				发送数据(玩家数据[id].连接id, 7, "#y/当前共有#r/" .. 在线人数 .. "#y/个玩家在线")

				return 0
			end

			if string.find(内容.内容, "@") == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/错误的游戏指令！")

				return 0
			else
				self.指令数据 = 分割文本(内容.内容, "@")

				if self.指令数据[1] == "jcrz" then
					if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					end

					f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "qq", self.指令数据[3])
					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "已经取消了认证状态")

					return 0
				elseif self.指令数据[1] == "zhxx" then
					if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					end

					发送数据(玩家数据[id].连接id, 9, self.指令数据[2] .. "账号信息如下：")
					发送数据(玩家数据[id].连接id, 9, " 密码：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "密码"))
					发送数据(玩家数据[id].连接id, 9, " 安全码：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "安全码"))
					发送数据(玩家数据[id].连接id, 9, " 仙玉：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉"))
					发送数据(玩家数据[id].连接id, 9, " 绑定qq：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "qq"))
					发送数据(玩家数据[id].连接id, 9, " 注册ip：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "ip"))
					发送数据(玩家数据[id].连接id, 9, " 注册时间：" .. f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "注册时间"))

					return 0
				elseif self.指令数据[1] == "zszq" then

					if 账号取id(self.指令数据[2]) == 0 or 玩家数据[账号取id(self.指令数据[2])] == nil then
						发送数据(玩家数据[id].连接id, 7, "#y/赠送坐骑需要对方在线")

						return 0
					end

					玩家数据[账号取id(self.指令数据[2])].角色.数据.备用坐骑 = self.指令数据[3]

					发送数据(玩家数据[id].连接id, 7, "#y/赠送坐骑成功")
					发送数据(玩家数据[账号取id(self.指令数据[2])].连接id, 7, "#y/你获得了新坐骑，按下F2后可骑乘该坐骑")

					return 0
				elseif self.指令数据[1] == "xgmm" then
					if f函数.文件是否存在(data目录 .. self.指令数据[2].. _账号txt) == false then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					end

					f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "密码", self.指令数据[3])
					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "密码修改成功")

					return 0
				elseif self.指令数据[1] == "jfzh" then
					if f函数.文件是否存在(data目录 .. self.指令数据[2] .. _角色txt) == false then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					end

					f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "封禁", "0")
					发送数据(玩家数据[id].连接id, 7, "#y/解封账号成功")

					return 0
				elseif self.指令数据[1] == "fjzh" then
					f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "封禁", "1")
					发送数据(玩家数据[id].连接id, 7, "#y/封禁账号成功")

					if 账号取id(self.指令数据[2]) == 0 then
						发送数据(玩家数据[id].连接id, 7, "#y/账号不存在或未上线")

						return 0
					else
						网络处理类:强制断开1(玩家数据[账号取id(self.指令数据[2])].连接id, "你的账号已经被管理员封禁，你已经被强制断开连接。")
						登录处理类:玩家退出(账号取id(self.指令数据[2]))
						发送数据(玩家数据[id].连接id, 7, "#y/该账号已被强制下线")

						return 0
					end
				elseif self.指令数据[1] == "tczd" then
					if 账号取id(self.指令数据[2]) == 0 then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					elseif 玩家数据[账号取id(self.指令数据[2])].战斗 == 0 then
						发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "未处于战斗状态中")

						return 0
					else
						战斗准备类.战斗盒子[玩家数据[账号取id(self.指令数据[2])].战斗]:强制结束战斗()

						战斗准备类.战斗盒子[玩家数据[账号取id(self.指令数据[2])].战斗] = nil

						发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "退出了战斗，请重进游戏")

						return 0
					end
				elseif self.指令数据[1] == "cjzh" then
					登录处理类:创建玩家账号(self.指令数据[2], self.指令数据[3], id, 玩家数据[id].角色.数据.离线ip)
					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "账号创建成功")

					return 0
				elseif self.指令数据[1] == "ghzx" then
					if 角色武器类型[self.指令数据[2]].主武器 == nil then
						发送数据(玩家数据[id].连接id, 7, "#y/这种造型无法进行转换")

						return 0
					elseif 玩家数据[id].角色.数据.装备数据[21] ~= nil then
						发送数据(玩家数据[id].连接id, 7, "#y/请先卸下所佩戴的武器")

						return 0
					end

					玩家数据[id].角色.数据.造型 = self.指令数据[2]

					网络处理类:强制断开1(玩家数据[id].连接id, "你的角色造型已经更换，请重新登录。")
				elseif self.指令数据[1] == "zbhq" then
					玩家数据[id].装备:取150级装备礼包(id)
					elseif self.指令数据[1] == "djzb" then
					玩家数据[id].装备:取160级装备礼包(id)
				elseif self.指令数据[1] == "zjxy" then
					self.可用点数 = f函数.读配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数") + 0
					self.指令数据[3] = self.指令数据[3] + 0
					self.消除点数 = (self.指令数据[3] + 0) / 10

					if self.可用点数 < self.消除点数 then
						发送数据(玩家数据[id].连接id, 7, "#y/你的可用点数不足，请联系管理员充值")

						return 0
					elseif f函数.文件是否存在(data目录 .. self.指令数据[2] .. _角色txt) == false then
						发送数据(玩家数据[id].连接id, 7, "#y/该账号不存在！")

						return 0
					elseif self.指令数据[3] < 0 then
						发送数据(玩家数据[id].连接id, 7, "#y/添加的仙玉不允许为负数")

						return 0
					end

					self.可用点数 = self.可用点数 - self.消除点数

					发送数据(玩家数据[id].连接id, 7, "#y/您账号可用点数为#r/" .. self.可用点数 .. "#y/点")

					self.临时仙玉 = f函数.读配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉") + 0

					if self.指令数据[3] >= 5000 and self.指令数据[3] < 10000 then
						self.指令数据[3] = math.floor(self.指令数据[3] * 1.2)
					elseif self.指令数据[3] >= 10000 then
						self.指令数据[3] = math.floor(self.指令数据[3] * 1.5)
					end

					self.临时仙玉 = self.临时仙玉 + self.指令数据[3]

					f函数.写配置(data目录 .. self.指令数据[2].. _账号txt, "账号信息", "仙玉", self.临时仙玉)
					f函数.写配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "点数", self.可用点数)
					发送数据(玩家数据[id].连接id, 7, "#y/" .. self.指令数据[2] .. "增加了#r/" .. self.指令数据[3] .. "#y/点仙玉")
					玩家数据[id].角色:添加消费日志(self.指令数据[2] .. "充值仙玉" .. self.指令数据[3] .. "点")
				end

				return 0
			end
		end
	end

	if string.find(内容.内容, "pk") ~= nil then
		self.战斗参数 = 分割文本(内容.内容, "pk")

		if self.战斗参数[2] ~= nil and tonumber(self.战斗参数[2]) then
			self.战斗参数 = self.战斗参数[2] + 0

			if 玩家数据[self.战斗参数] == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/这个玩家不存在或没在线")

				return 0
			else
				玩家数据[id].战斗对象 = self.战斗参数
				self.对话内容 = " 你将花费150点人气对#Y/" .. 玩家数据[self.战斗参数].角色.数据.等级 .. "#W/级的#G/" .. 玩家数据[self.战斗参数].角色.数据.名称 .. [[
#W/发起战斗，若对方等级低于或高于你10级将额外扣除150点人气。pk保护期内的玩家可选强行切入，强行切入会让pk保护期内的玩家失去保护，但需要额外扣除300点人气。请确认是否要发起战斗：

  #R/ht|pkzd/普通切入(对保护期内的玩家无效)
  #R/ht|pkzd1/强行切入(无视对方保护期，如果对方处于保护期内将额外消耗300人气)
  #R/ht|0/我再考虑会


     ]]
				self.发送信息 = {
					编号 = Q_人物头像[玩家数据[id].角色.数据.造型],
					对话 = self.对话内容,
					名称 = 玩家数据[id].角色.数据.名称
				}

				发送数据(玩家数据[id].连接id, 20, table.tostring(self.发送信息))

				return 0
			end
		end
	end

	if 内容.参数 == 3 then
		self.允许发送 = false

		if 玩家数据[id].角色.数据.等级 < 20 then
			发送数据(玩家数据[id].连接id, 7, "#y/只有等级达到20级的玩家才可使用此频道")

			return 0
		end

		if 玩家数据[id].付费用户 then
			if os.time() - 玩家数据[id].世界频道 <= 30 then
				发送数据(玩家数据[id].连接id, 7, "#y/您距离登录或上一次发言间隔时间未到30秒")
			else
				玩家数据[id].世界频道 = os.time()
				self.允许发送 = true
			end
		else
			if 银子检查(id, 2000) == false then
				发送数据(玩家数据[id].连接id, 7, "#y/非付费版在此频道发言每次需要消耗2000两银子")

				return 0
			elseif 玩家数据[id].角色.数据.当前活力 < 20 then
				发送数据(玩家数据[id].连接id, 7, "#y/非付费版在此频道发言每次需要消耗20点活力")

				return 0
			elseif os.time() - 玩家数据[id].世界频道 <= 20 then
				发送数据(玩家数据[id].连接id, 7, "#y/您距离登录或上一次发言间隔时间未到20秒")

				return 0
			end

			玩家数据[id].角色.数据.当前活力 = 玩家数据[id].角色.数据.当前活力 - 20

			玩家数据[id].角色:扣除银子(id, 2000, 10)

			玩家数据[id].世界频道 = os.time()

			发送数据(玩家数据[id].连接id, 9, " #y/失去2000两银子")
			发送数据(玩家数据[id].连接id, 9, " #y/失去20点活力")

			self.允许发送 = true
		end

		if self.允许发送 then
			self.发送消息 = "#sj/#w/[" .. 玩家数据[id].角色.数据.名称 .. "]" .. 内容.内容

			广播消息(9, self.发送消息, id)

			if 三界书院.开关 and 三界书院.答案 == 内容.内容 then
				self.名单重复 = false

				for n = 1, #三界书院.名单 do
					if 三界书院.名单[n].id == id then
						self.名单重复 = true
					end
				end

				if self.名单重复 == false then
					三界书院.名单[#三界书院.名单 + 1] = {
						id = id,
						名称 = 玩家数据[id].角色.数据.名称,
						用时 = os.time() - 三界书院.起始
					}
				end
			end
		end
	elseif 内容.参数 == 1 then
		if 玩家数据[id].付费用户 then
			if os.time() - 玩家数据[id].当前频道 <= 5 then
				发送数据(玩家数据[id].连接id, 7, "#y/您距离登录或上一次发言间隔时间未到5秒")

				return 0
			end
		elseif os.time() - 玩家数据[id].当前频道 <= 5 then
			发送数据(玩家数据[id].连接id, 7, "#y/您距离登录或上一次发言间隔时间未到5秒")

			return 0
		end

		玩家数据[id].当前频道 = os.time()

		地图处理类:发送当前消息(id, 内容.内容)
		发送数据(玩家数据[id].连接id, 1014, table.tostring({
			消息 = 内容.内容,
			id = 玩家数据[id].数字id
		}))
	elseif 内容.参数 == 2 then
		if 玩家数据[id].队伍 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/请先加入一个队伍")

			return 0
		elseif os.time() - 玩家数据[id].队伍频道 <= 2 then
			发送数据(玩家数据[id].连接id, 7, "#y/您距离登录或上一次发言间隔时间未到10秒")

			return 0
		else
			玩家数据[id].队伍频道 = os.time()
			self.组合消息 = "#dw/#w/[" .. 玩家数据[id].角色.数据.名称 .. "]" .. 内容.内容

			发送数据(玩家数据[id].连接id, 1014, table.tostring({
				消息 = 内容.内容,
				id = 玩家数据[id].数字id
			}))
			发送数据(玩家数据[id].连接id, 9, self.组合消息)

			for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
				if 队伍数据[玩家数据[id].队伍].队员数据[n] ~= id then
					发送数据(玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].连接id, 9, self.组合消息)
					发送数据(玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].连接id, 1014, table.tostring({
						消息 = 内容.内容,
						id = 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].数字id
					}))
				end
			end
		end
	end
end

function 聊天处理类:更新(dt)
end

function 聊天处理类:显示(x, y)
end

return 聊天处理类
