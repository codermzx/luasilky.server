local 地图处理类 = class()

function 地图处理类:名称转编号(名称)
	for n = 1, #self.地图id do
		if self.地图数据[self.地图id[n]].名称 == 名称 then
			return self.地图id[n]
		end
	end

	return 0
end

function 地图处理类:初始化()
 self.地图id={6001,6002,6003,4131,4132,4133,3131,3132,5135,5134,1125,1004,1005,1006,1007,1008,1040,1090,5131,1226,1501,1504,1505,1511,1506,1503,1507,1508,1509,1524,1532,1537,1126,1092,1139,1142,1514,1174,1177,1178,1179,1180,1181,1182,1183,1186,1187,1188,1189,1190,1191,1192,1091,1111,1070,1135,1173,1131,1512,
 1513,1146,1201,1202,1203,1204,1205,1207,1208,1110,1140,1122,1127,1128,1129,1130,1202,1001,1003,1198,1002,1193,1116,1117,1118,1119,1120,1121,1138,1154,1156,1505,1504,1112,1114,1117,1141,1147,1150,1054,1043,1143,1137,1134,1144,1145,1123,1124,1226,1228,1875,
 1860,1845,1835,1825,1815,1865,1232,1233,1235,1242,1041,1210,1042,1218,1211,1221,1511,1229,1231,2000,1175,1028,1015,1216,1213,1916,1920}
	self.地图玩家 = {}
	self.地图数据 = {}
	printf('.')
	-- __S服务:输出("开始加载地图数据……")

	for n = 1, #self.地图id, 1 do
		local mapid = self.地图id[n]
		self.地图玩家[mapid] = {}
		if mapid ~= 5135 and mapid ~= 6001 and mapid ~= 6002 and mapid ~= 6003 then
			self.文件地址 = "map\\" .. self.地图id[n] .. ".txt"
			self.临时代码 = 读入文件(self.文件地址)
			self.临时函数 = load(self.临时代码)--loadstring(self.临时代码)
			self.临时函数()

			
			self.地图数据[mapid] = table.copy(dtsjz)
			self.地图数据[mapid].坐标 = 地图坐标类.创建(mapid)
		else
			self.地图数据[mapid] = {}
			if mapid == 5135 then
				self.地图数据[mapid].名称 = "门派比武场"
				self.地图数据[mapid].音乐 = "84E2EB78"
				self.地图数据[mapid].传送圈 = {}
				self.地图数据[mapid].坐标 = 地图坐标类.创建(5131)
			elseif mapid == 6001 then
				self.地图数据[mapid].名称 = "精锐比武场"
				self.地图数据[mapid].音乐 = "BED44FE1"
				self.地图数据[mapid].传送圈 = {}
				self.地图数据[mapid].坐标 = 地图坐标类.创建(5131)
			elseif mapid == 6002 then
				self.地图数据[mapid].名称 = "神威比武场"
				self.地图数据[mapid].音乐 = "BED44FE1"
				self.地图数据[mapid].传送圈 = {}
				self.地图数据[mapid].坐标 = 地图坐标类.创建(5131)
			elseif mapid == 6003 then
				self.地图数据[mapid].名称 = "天科比武场"
				self.地图数据[mapid].音乐 = "BED44FE1"
				self.地图数据[mapid].传送圈 = {}
				self.地图数据[mapid].坐标 = 地图坐标类.创建(5131)
			end
		end
		self.地图数据[mapid].编号 = self.地图id[n]
		self.地图数据[mapid].单位组 = {}


		if mapid ~= 1001 and self.地图数据[mapid].音乐 == '26275208' then
			-- __S服务:输出(self.地图数据[self.地图id[n]].名称)
			if 地图数据类.数据[mapid] and 地图数据类.数据[mapid].音乐 then
				-- __S服务:输出(string.format('0x%08X', 地图数据类.数据[self.地图id[n]].音乐))
				self.地图数据[mapid].音乐 = 地图数据类.数据[mapid].音乐
			end
		end
	end

	for n = 1, 20 do
		self.临时id = n + 7000
		self.地图玩家[self.临时id] = {}
		self.地图数据[self.临时id] = {
			名称 = "幻域" .. self.临时id - 7000 .. "层",
			音乐 = "266E2C58",
			传送圈 = {},
			坐标 = 地图坐标类.创建(1193),
			编号 = self.临时id,
			单位组 = {}
		}
	end
	printf('.')
	-- __S服务:输出("加载地图数据结束……")

end

function 地图处理类:取迷宫玩家组()
	self.返回玩家组 = {}

	for n = 1, 20 do
		self.临时id = n + 7000

		for i, v in pairs(self.地图玩家[self.临时id]) do
			if 玩家数据[i] ~= nil and 玩家数据[i].战斗 == 0 then
				self.返回玩家组[#self.返回玩家组 + 1] = i
			end
		end
	end

	return self.返回玩家组
end

function 地图处理类:重置比武大会玩家()
	比武大会数据 = {}

	for n = 6001, 6003 do
		self.临时id = n

		for i, v in pairs(self.地图玩家[self.临时id]) do
			玩家数据[i].比武保护期 = 0
			比武大会数据[i] = {
				奖励 = false,
				积分 = 0
			}

			发送数据(玩家数据[i].连接id, 7, "#y/你当前的比武积分为0分")
			发送数据(玩家数据[i].连接id, 7, "#y/每战胜一名玩家可获得5点积分")
			-- __S服务:发送(玩家数据[i].连接id, 700, 5)
		end
	end

end

function 地图处理类:传出幻域迷宫()
	self.返回玩家组 = {}

	for n = 1, 20 do
		self.临时id = n + 7000

		for i, v in pairs(self.地图玩家[self.临时id]) do
			if 玩家数据[i].地图 > 7000 and 玩家数据[i].地图 <= 7020 then
				-- Nothing
			end

			if 玩家数据[i] ~= nil and 玩家数据[i].战斗 == 0 then
				self.返回玩家组[#self.返回玩家组 + 1] = i
			end
		end
	end

	return self.返回玩家组
end

function 地图处理类:无间炼狱奖励()
	for n, v in pairs(self.地图玩家[5131]) do
		if 玩家数据[n] ~= nil then
			self.临时等级 = 玩家数据[n].角色.数据.等级
			self.临时经验 = self.临时等级 * 500
			self.临时银子 = 10000

			玩家数据[n].角色:添加经验(self.临时经验, n, 7, 0)
			玩家数据[n].角色:添加储备(n, self.临时银子, 10)

			if 取随机数() <= 5 and 玩家数据[n].角色:取任务id(6) ~= 0 and 任务数据[玩家数据[n].角色:取任务id(6)] ~= nil and 任务数据[玩家数据[n].角色:取任务id(6)].分类 == 3 and 任务数据[玩家数据[n].角色:取任务id(6)].传说 == nil then
				self.临时id = 玩家数据[n].装备:生成装备(任务数据[玩家数据[n].角色:取任务id(6)].打造类型, 任务数据[玩家数据[n].角色:取任务id(6)].道具, 任务数据[玩家数据[n].角色:取任务id(6)].等级, 1, 1, false, 0, 0, nil, nil)
				self.临时格子 = 玩家数据[n].角色:取可用道具格子("包裹")

				发送数据(玩家数据[n].连接id, 7, "#y/你获得了#r/" .. 任务数据[玩家数据[n].角色:取任务id(6)].道具)

				if self.临时格子 ~= 0 then
					玩家数据[n].角色.数据.道具.包裹[self.临时格子] = self.临时id
				else
					-- 临时背包处理
					发送数据(玩家数据[id].连接id, 7, "#y/由于你的包裹空间已满，无法获得新道具#r/" .. 任务数据[玩家数据[n].角色:取任务id(6)].道具)
				end

				任务数据[玩家数据[n].角色:取任务id(6)].传说 = 1

				玩家数据[n].角色:添加系统消息(n, "#w/你获得了传说中的#r/" .. 任务数据[玩家数据[n].角色:取任务id(6)].道具)
			end
		end
	end
end

function 地图处理类:输出地图单位(地图)
	self.输出信息 = "数据生成#换行符"

	for a, b in pairs(self.地图数据[地图].单位组) do
		if self.地图数据[地图].单位组[a] == nil then
			self.输出信息 = self.输出信息 .. "该单位数据为nil,单位编号" .. a .. "#换行符#换行符"
		else
			self.输出信息 = self.输出信息 .. "[" .. a .. "]#换行符"

			for n, v in pairs(self.地图数据[地图].单位组[a]) do
				if type(v) == "table" then
					self.输出信息 = self.输出信息 .. n .. "：" .. table.tostring(v) .. "#换行符"
				elseif v == nil then
					self.输出信息 = self.输出信息 .. n .. "：nil#换行符"
				elseif v == true then
					self.输出信息 = self.输出信息 .. n .. "：true#换行符"
				elseif v == false then
					self.输出信息 = self.输出信息 .. n .. "：false#换行符"
				else
					self.输出信息 = self.输出信息 .. n .. "：" .. v .. "#换行符"
				end
			end

			self.输出信息 = self.输出信息 .. "#换行符#换行符"
		end
	end

	当前时间 = os.time()
	当前年份 = os.date("%Y", 当前时间)
	当前月份 = os.date("%m", 当前时间)
	当前日份 = os.date("%d", 当前时间)
	if f函数.文件是否存在(程序目录..[[log\]]..当前年份)==false then
	  创建目录([[log\]]..当前年份)
	end
	if f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份)==false then
	  创建目录([[log\]]..当前年份..[[\]]..当前月份)
	end
	if f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份)==false or f函数.文件是否存在(程序目录..[[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."在线日志")==false then
	  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份)
	  创建目录([[log\]]..当前年份..[[\]]..当前月份..[[\]]..当前日份..[[\]].."在线日志")
	end
	写出文件("log/" .. 当前年份 .. "/" .. 当前月份 .. "/" .. 当前日份 .. "/" .. "在线日志" .. "/" .. 地图 .. "单位数据.txt", self.输出信息)
end

function 地图处理类:添加单位(数据组)
	self.临时编号 = #self.地图数据[数据组.地图编号].单位组 + 1
	self.地图数据[数据组.地图编号].单位组[self.临时编号] = {
		名称 = 数据组.名称,
		造型 = 数据组.造型,
		方向 = 数据组.方向,
		编号 = 数据组.编号,
		坐标 = 数据组.坐标,
		战斗 = 数据组.战斗,
		id = 数据组.任务id,
		染色 = 数据组.染色,
		武器 = 数据组.武器,
		标识 = self.临时编号,
		副本 = 数据组.副本,
		称谓 = 数据组.称谓,
		变异 = 数据组.变异

	}

	self:发送指定地图单位(数据组.地图编号, self.临时编号)
end

function 地图处理类:移除单位(地图, 移除id)
	self.标记单位 = 0
	地图 = 地图 + 0

	for n, v in pairs(self.地图数据[地图].单位组) do
		if self.地图数据[地图].单位组[n].id == 移除id then
			self.地图数据[地图].单位组[n] = nil
			self.标记单位 = n
		end
	end

	if self.标记单位 ~= 0 then
		for n, v in pairs(self.地图玩家[地图]) do
			if 玩家数据[n] ~= nil then
				发送数据(玩家数据[n].连接id, 1018, 移除id)
			end
		end
	end
end

function 地图处理类:发送地图单位(id, 地图)
	if 地图 ~= 3131 and 地图 ~= 3132 then
		for n, v in pairs(self.地图数据[地图].单位组) do
			发送数据(玩家数据[id].连接id, 1017, self.地图数据[地图].单位组[n])
		end
	else
		self.发送单位信息 = {}

		for n, v in pairs(self.地图数据[地图].单位组) do
			if self.地图数据[地图].单位组[n] ~= nil and self.地图数据[地图].单位组[n].副本 == 玩家数据[id].副本 then
				self.发送单位信息[n] = self.地图数据[地图].单位组[n]
			end
		end

		发送数据(玩家数据[id].连接id, 1019, self.发送单位信息)
	end
end

function 地图处理类:单独发送地图单位(地图, 编号, id)
	for n, v in pairs(self.地图玩家[地图]) do
		if 玩家数据[n] ~= nil and self.地图数据[地图].单位组[编号] ~= nil and (self.地图数据[地图].单位组[编号].副本 == nil or self.地图数据[地图].单位组[编号].副本 == 玩家数据[n].副本) then
			发送数据(玩家数据[n].连接id, 1017, self.地图数据[地图].单位组[编号])
		end
	end
end

function 地图处理类:发送指定地图单位(地图, 编号, id)
	for n, v in pairs(self.地图玩家[地图]) do
		if 玩家数据[n] ~= nil and self.地图数据[地图].单位组[编号] ~= nil and (self.地图数据[地图].单位组[编号].副本 == nil or self.地图数据[地图].单位组[编号].副本 == 玩家数据[n].副本) then
			发送数据(玩家数据[n].连接id, 1017, self.地图数据[地图].单位组[编号])
		end
	end
end

function 地图处理类:数据处理(内容)
	self.处理信息 = 内容 --table.loadstring(内容)
	if 玩家数据[self.处理信息.数字id] == nil then
		return 0
	end
	if 玩家数据[self.处理信息.数字id].摆摊 ~= nil then
		return 0
	end

	if self.处理信息.序号 == 1 then
		self:玩家移动请求(self.处理信息.数字id, self.处理信息.文本)
	elseif self.处理信息.序号 == 2 then
		self:玩家坐标刷新(self.处理信息.数字id, self.处理信息)
	elseif self.处理信息.序号 == 3 then
		self:玩家坐标刷新(self.处理信息.数字id, self.处理信息)
	elseif self.处理信息.序号 == 4 then
		self:玩家坐标刷新(self.处理信息.数字id, self.处理信息)
		self:玩家坐标刷新(self.处理信息.数字id, self.处理信息)
	elseif self.处理信息.序号 == 5 then
		self:玩家移动请求(self.处理信息.数字id, self.处理信息.文本)
	elseif self.处理信息.序号 == 6 then
		if 玩家数据[self.处理信息.数字id].队伍 == 0 or 玩家数据[self.处理信息.数字id].队长 then
			self:跳转处理(self.处理信息.数字id, 6, self.处理信息.参数)
		end
	elseif self.处理信息.序号 == 7 then
		self:玩家移动请求(self.处理信息.数字id, self.处理信息.文本, 1)
	end
end

function 地图处理类:跳转处理(id, 序号, 消息)
	消息 = 消息 + 0
	self.原始地图 = 玩家数据[id].地图
	self.发送消息1 = {
		编号 = self.地图数据[self.原始地图].传送圈[消息].目标,
		x = self.地图数据[self.原始地图].传送圈[消息].目标x * 20,
		y = self.地图数据[self.原始地图].传送圈[消息].目标y * 20
	}

	if 玩家数据[id].队伍 == 0 then
		self:指定跳转(id, self.发送消息1.编号, self.发送消息1.x, self.发送消息1.y)
	else
		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			self.发送消息1 = {
				编号 = self.地图数据[self.原始地图].传送圈[消息].目标,
				x = self.地图数据[self.原始地图].传送圈[消息].目标x * 20,
				y = self.地图数据[self.原始地图].传送圈[消息].目标y * 20
			}

			self:指定跳转(队伍数据[玩家数据[id].队伍].队员数据[n], self.发送消息1.编号, self.发送消息1.x, self.发送消息1.y)
		end
	end
end

function 地图处理类:指定跳转1(id, 地图编号, 地图x, 地图y, 战斗)
	if 玩家数据[id].战斗 ~= 0 and 战斗 == nil then
		return 0
	end

	if 玩家数据[id].队伍 == 0 then
		self:指定跳转(id, 地图编号, 地图x * 20, 地图y * 20)
	else
		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			self:指定跳转(队伍数据[玩家数据[id].队伍].队员数据[n], 地图编号, 地图x * 20, 地图y * 20)
		end
	end
end

function 地图处理类:指定跳转(id, 地图编号, 地图x, 地图y)
	self:移除玩家(玩家数据[id].地图, id)

	玩家数据[id].地图 = 地图编号

	if 地图编号 == 1501 or 地图编号 == 1001 or 地图编号 == 1070 or 地图编号 == 1092 or 地图编号 == 1208 or 地图编号 == 1226 or 地图编号 == 1040 then
		玩家数据[id].角色.数据.传送地图 = 地图编号
	end

	玩家数据[id].角色.数据.地图数据.编号 = 地图编号
	玩家数据[id].角色.数据.地图数据.x = 地图x
	玩家数据[id].角色.数据.地图数据.y = 地图y
	self:加入玩家(id)
	self.发送消息 = {x = 地图x,y = 地图y}
	发送数据(玩家数据[id].连接id,2014, self.发送消息)
end

function 地图处理类:玩家停止移动(id, 内容)
	self.发送内容 = {
		数据 = 内容,
		id = id
	}

	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id,1006,self.发送内容)
		end
	end
end

function 地图处理类:升级事件(id)
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id, 1007, id)
		end
	end
end

function 地图处理类:玩家移动请求(id, 内容, 队伍)
	if 玩家数据[id] == nil then
		return 0
	end

	if 队伍 == nil and 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 == false then
		return 0
	end

	local 临时路径 = 内容

	发送数据(玩家数据[id].连接id,1002,内容)

	玩家数据[id].移动目标 = 内容
	self.发送内容 = {
		数据 = 内容,
		id = id
	}

	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 玩家数据[id].地图) then
			发送数据(玩家数据[n].连接id,1005,self.发送内容)
		end
	end

	if 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 then
		self.临时文本 = 分割文本(内容, "*-*")

		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			if n ~= 1 then
				发送数据(玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].连接id, 1020, {
					目标 = 玩家数据[id].移动目标,
					x1 = self.临时文本[1],
					x2 = self.临时文本[2],
					序列 = (n - 1) * 2
				})
			end
		end
	end

	if 2 == 1 and 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 then
		内容 = table.loadstring(内容)

		if 内容 == "" or 内容 == nil then
			return 0
		end

		for n = 2, #队伍数据[玩家数据[id].队伍].队员数据 do
			self.路径编号 = 0

			for a = #内容 - (n + 2), 1, -1 do
				if self.路径编号 == 0 and 取两点距离a(玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].角色.数据.地图数据.x, 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].角色.数据.地图数据.y, 内容[a].x * 20, 内容[a].y * 20) >= (n - 1) * 50 then
					self.路径编号 = a
				end
			end

			self.发送内容 = {
				数据 = {}
			}

			if self.路径编号 ~= 0 then
				for a = 1, self.路径编号 do
					self.发送内容.数据[a] = 内容[a]
				end
			end

			发送数据(玩家数据[队伍数据[玩家数据[id].队伍].队员数据[n]].连接id, 1011, table.tostring(self.发送内容.数据))

			self.发送内容.id = 队伍数据[玩家数据[id].队伍].队员数据[n]

			for i, v in pairs(self.地图玩家[玩家数据[id].地图]) do
				if i ~= 队伍数据[玩家数据[id].队伍].队员数据[n] and 玩家数据[i] ~= nil and self:取同一地图(id, i, 玩家数据[id].地图) then
					发送数据(玩家数据[i].连接id, 1005, table.tostring(self.发送内容))
				end
			end
		end
	end
end

function 地图处理类:玩家坐标刷新(id, 内容)
	内容.x = 内容.参数
	内容.y = 内容.文本 + 0

	if 内容 == nil or 内容 == "" then
		return 0
	end

	if 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 == false then
		self.队员编号 = 0

		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			if 队伍数据[玩家数据[id].队伍].队员数据[n] == id then
				self.队员编号 = n
			end
		end

		if self.队员编号 ~= 0 then
			if 取两点距离a(内容.x, 内容.y, 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[1]].角色.数据.地图数据.x, 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[1]].角色.数据.地图数据.y) >= (self.队员编号 - 1) * 50 then
				-- Nothing
			else
				return 0
			end
		end
	end

	玩家数据[id].角色.数据.地图数据.x = 内容.x
	玩家数据[id].角色.数据.地图数据.y = 内容.y

	if 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 then
		-- Nothing
	elseif 玩家数据[id].队伍 ~= 0 then
		self.队员编号 = 0

		for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
			if 队伍数据[玩家数据[id].队伍].队员数据[n] == id then
				self.队员编号 = n
			end
		end

		if self.队员编号 ~= 0 and 取两点距离a(内容.x, 内容.y, 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[1]].角色.数据.地图数据.x, 玩家数据[队伍数据[玩家数据[id].队伍].队员数据[1]].角色.数据.地图数据.y) >= (self.队员编号 - 1) * 50 then
			self:玩家移动请求(id, 内容.x .. "*-*" .. 内容.y)
		end
	end

	if 天罚数据表[id] ~= nil and 天罚数据表[id] >= 3 and 取随机数() <= 5 then
		玩家数据[id].角色:接受天罚(id)
	elseif self.地图数据[玩家数据[id].地图].怪物 ~= nil and (玩家数据[id].队伍 == 0 or 玩家数据[id].队长) and 玩家数据[id].遇怪时间.间隔 <= os.time() - 玩家数据[id].遇怪时间.起始 and 玩家数据[id].战斗 == 0 and 玩家数据[id].角色:取任务id(1) == 0 then
		if 玩家数据[id].角色:取任务id(16) == 0 or self:取门派地图(id) == false then
			玩家数据[id].遇怪时间.起始 = os.time()

			战斗准备类:创建战斗(id, 100001, 0)
		end
	elseif 玩家数据[id].角色:取任务id(16) ~= 0 and self:取门派地图(id) and 任务数据[玩家数据[id].角色:取任务id(16)].分类 == 4 and 任务数据[玩家数据[id].角色:取任务id(16)].战斗 <= os.time() - 任务数据[玩家数据[id].角色:取任务id(16)].起始 and 任务数据[玩家数据[id].角色:取任务id(16)].次数 > 0 then
		任务数据[玩家数据[id].角色:取任务id(16)].起始 = os.time()

		战斗准备类:进入处理(id, 100014, "66", 玩家数据[id].角色:取任务id(16))
	end
 end
function 地图处理类:取门派地图(id)
 local 门派巡逻地图={
	大唐官府={1198,1054,1052}
	,化生寺={1043,1002,1528}
	,方寸山={1135,1137}
	,女儿村={1143,1142}
	,龙宫={1117,1116}
	,天宫={1111,1112}
	,普陀山={1140,1141}
	,五庄观={1146,1147}
	,魔王寨={1512,1145}
	,盘丝洞={1513,1144}
	,阴曹地府={1125,1124,1123,1122}
	,狮驼岭={1131,1134,1132,1133}
	,凌波城={1150}
	,神木林={1138,1154}
	,无底洞={1139,1156}}
	if 玩家数据[id].角色.数据.门派 == "无" then
		return false
	else
		for n = 1, #门派巡逻地图[玩家数据[id].角色.数据.门派] do
			if 玩家数据[id].角色.数据.地图数据.编号 == 门派巡逻地图[玩家数据[id].角色.数据.门派][n] then
				return true
			end
		end

		return false
	end
 end
function 地图处理类:加入玩家1(id)
	if 玩家数据[id] == nil then
		return 0
	elseif 玩家数据[id].地图 == nil or self.地图玩家[玩家数据[id].地图] == nil then
		return 0
	end
	self.地图玩家[玩家数据[id].地图][id] = os.time()
	-- self.发送消息 = {
	-- 	音乐 = self.地图数据[玩家数据[id].地图].音乐,
	-- 	编号 = 玩家数据[id].地图,
	-- 	名称 = self.地图数据[玩家数据[id].地图].名称,
	-- 	传送圈 = self.地图数据[玩家数据[id].地图].传送圈
	-- }

	发送数据(玩家数据[id].连接id,1001,玩家数据[id].地图) --self.发送消息
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 玩家数据[id].地图) then
			发送数据(玩家数据[id].连接id, 1004, 玩家数据[n].角色:获取地图数据())

			if 玩家数据[n].战斗 ~= 0 then
				发送数据(玩家数据[id].连接id, 1012, n)
			end
		end
	end

	-- self:获取npc(id)
	self:发送地图单位(id, 玩家数据[id].地图)
 end
function 地图处理类:加入玩家(id)
	if 玩家数据[id] == nil then
		return 0
	end
	玩家数据[id].遇怪时间 = {
		起始 = os.time(),
		间隔 = 取随机数(1, 2)
	}
	self.地图玩家[玩家数据[id].地图][id] = os.time()
	-- self.发送消息 = {
	-- 	音乐 = self.地图数据[玩家数据[id].地图].音乐,
	-- 	编号 = 玩家数据[id].地图,
	-- 	名称 = self.地图数据[玩家数据[id].地图].名称,
	-- 	传送圈 = self.地图数据[玩家数据[id].地图].传送圈
	-- }
	发送数据(玩家数据[id].连接id,1001,玩家数据[id].地图) --self.发送消息
	self:发送玩家(id, 玩家数据[id].地图)
	self:发送地图单位(id, 玩家数据[id].地图)
	-- self:获取npc(id)
 end
function 地图处理类:更新摊位(id, 名称, 地图)
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 玩家数据[id].地图) then
			发送数据(玩家数据[n].连接id, 20009, {id = id,名称 = 名称})
		end
	end
 end
function 地图处理类:获取npc(id)
	self.发送信息 = {}

	if 地图数据类.数据[玩家数据[id].地图] ~= nil and 地图数据类.数据[玩家数据[id].地图].npc ~= nil then
		for n = 1, #地图数据类.数据[玩家数据[id].地图].npc do
			self.发送信息[n] = table.copy(地图数据类.npc[地图数据类.数据[玩家数据[id].地图].npc[n]])
			-- if self.发送信息[n].名称 == "帮派管事" and 玩家数据[id].帮派进入编号 ~= nil then
			-- 	self.发送信息[n].名称 = 帮派数据[玩家数据[id].帮派进入编号].名称 .. "总管"
			-- end
		end
	end
	发送数据(玩家数据[id].连接id,1010,self.发送信息)
end

function 地图处理类:更改队长(id, 队长)
	if 队长 then
		self.发送序号 = 5004
	else
		self.发送序号 = 5005
	end
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id, self.发送序号, id)
		end
	end
 end
function 地图处理类:发送玩家(id, 地图)
	if self.地图数据[地图] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/地图数据错误，错误代号7001")

		return 0
	end

	for n, v in pairs(self.地图玩家[地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 地图) then
			发送数据(玩家数据[n].连接id,1004,玩家数据[id].角色:获取地图数据(id))
		end
	end

	for n, v in pairs(self.地图玩家[地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 地图) then
			发送数据(玩家数据[id].连接id,1004,玩家数据[n].角色:获取地图数据(n))
		end
	end
 end
function 地图处理类:取同一地图(id, id1, 地图)
	if 地图 == 5135 then
		if 玩家数据[id1].角色.数据.门派 ~= 玩家数据[id].角色.数据.门派 then
			return false
		end
	elseif 地图 == 1875 or 地图 == 1865 or 地图 == 1855 or 地图 == 1845 or 地图 == 1835 or 地图 == 1825 or 地图 == 1815 then
		if 玩家数据[id].帮派进入方式 == 1 then
			if 玩家数据[id].角色.数据.帮派 ~= 玩家数据[id1].角色.数据.帮派 then
				return false
			else
				return true
			end
		end
	elseif 地图 == 3131 or 地图 == 3132 or 地图 == 4131 or 地图 == 4132 or 地图 == 4133 then
		if 玩家数据[id1].副本 == 玩家数据[id].副本 then
			return true
		else
			return false
		end
	end

	return true
 end
function 地图处理类:发送数据(id, 序号, 内容, 地图)
	for n, v in pairs(self.地图玩家[地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id, 序号, 内容)
		end
	end
 end
function 地图处理类:摊位重复(id, 地图)
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil and self:取同一地图(id, n, 玩家数据[id].地图) and 玩家数据[n].摆摊 ~= nil and self:取距离范围(id, n, 50) then
			return true
		end
	end
	return false
 end
function 地图处理类:取距离范围(id, id1, 距离)
	local p1 = 玩家数据[id]
	local p2 = 玩家数据[id1]
	if p1 == nil or p2 == nil or p1.地图 ~= p2.地图 then
		return false
	end
	local d1 = p1.角色.数据.地图数据
	local d2 = p2.角色.数据.地图数据
	return 取两点距离a(d1.x, d1.y, d2.x, d2.y) <= 距离
 end
function 地图处理类:发送当前消息(id, 内容)
	local iplayer = 玩家数据[id]
	local irole = iplayer.角色.数据
	local imap = irole.地图数据
	self.组合消息 = "#dq/#w/[" .. irole.名称 .. "]" .. 内容
	发送数据(iplayer.连接id, 9, self.组合消息)
	local player
	for n, v in pairs(self.地图玩家[iplayer.地图]) do
		player = 玩家数据[n]
		if n ~= id and player ~= nil and self:取同一地图(id, n, iplayer.地图) and 取两点距离a(imap.x, imap.y, imap.x, player.角色.数据.地图数据.y) <= 2000 then
			-- 发送数据(玩家数据[n].连接id, 1015, {消息 = 内容,id = 玩家数据[id].数字id })
			-- 发送数据(玩家数据[n].连接id, 21, self.组合消息)
			发送数据(玩家数据[n].连接id, 23, {消息 = 内容,id = 玩家数据[id].数字id, name = irole.名称 })
		end
	end
 end
function 地图处理类:更改战斗(id, 战斗)
	if 战斗 then
		self.发送序列 = 1012
	else
		self.发送序列 = 1013
	end
	for n, v in pairs(self.地图玩家[玩家数据[id].地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id, self.发送序列, id)
		end
	end
 end
function 地图处理类:更改称谓(地图, id, 称谓)
	self.发送信息 = {称谓 = 称谓,id = id}
	for n, v in pairs(self.地图玩家[地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id, 1009, table.tostring(self.发送信息))
		end
	end
 end
function 地图处理类:移除玩家(地图, id)
	if self.地图玩家[地图] == nil then
		return 0
	end
	self.地图玩家[地图][id] = nil
	for n, v in pairs(self.地图玩家[地图]) do
		if n ~= id and 玩家数据[n] ~= nil then
			发送数据(玩家数据[n].连接id,1008,id)
		end
	end
 end
return 地图处理类
